'Imports NTBEdit.MainForm
Imports System.Data.SqlClient
Imports System.io
Imports System.Configuration.ConfigurationSettings

Public Class LoginForm
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtUserId As System.Windows.Forms.TextBox
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnLogin As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cbSites As System.Windows.Forms.ComboBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.txtUserId = New System.Windows.Forms.TextBox
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.btnLogin = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.cbSites = New System.Windows.Forms.ComboBox
        Me.SuspendLayout()
        '
        'txtUserId
        '
        Me.txtUserId.Location = New System.Drawing.Point(120, 24)
        Me.txtUserId.Name = "txtUserId"
        Me.txtUserId.Size = New System.Drawing.Size(120, 20)
        Me.txtUserId.TabIndex = 0
        Me.txtUserId.Text = ""
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(120, 48)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(120, 20)
        Me.txtPassword.TabIndex = 1
        Me.txtPassword.Text = ""
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(24, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 23)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Brukernavn:"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(24, 48)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 23)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Passord:"
        '
        'btnLogin
        '
        Me.btnLogin.Location = New System.Drawing.Point(120, 104)
        Me.btnLogin.Name = "btnLogin"
        Me.btnLogin.Size = New System.Drawing.Size(72, 23)
        Me.btnLogin.TabIndex = 3
        Me.btnLogin.Text = "Logg inn"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(24, 72)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 23)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Site:"
        '
        'cbSites
        '
        Me.cbSites.Location = New System.Drawing.Point(120, 72)
        Me.cbSites.Name = "cbSites"
        Me.cbSites.Size = New System.Drawing.Size(121, 21)
        Me.cbSites.TabIndex = 2
        '
        'LoginForm
        '
        Me.AcceptButton = Me.btnLogin
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(272, 134)
        Me.Controls.Add(Me.cbSites)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btnLogin)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.txtUserId)
        Me.Name = "LoginForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Login NTBEdit"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public conString As String

    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Dim frmMain As New MainForm
        Dim dbNTB As New SqlConnection(conString)
        Dim cd As New SqlCommand("_CheckUserLogin", dbNTB)

        cd.CommandType = CommandType.StoredProcedure
        cd.Parameters.Add(New SqlParameter("@userid", SqlDbType.VarChar, 20))
        cd.Parameters.Item("@userid").Value = txtUserId.Text
        cd.Parameters.Add(New SqlParameter("@password", SqlDbType.VarChar, 20))
        cd.Parameters.Item("@password").Value = txtPassword.Text
        cd.Parameters.Add(New SqlParameter("@maingroup", SqlDbType.Int))
        cd.Parameters.Item("@maingroup").Value = -1
        cd.Parameters.Add(New SqlParameter("@isadm", SqlDbType.TinyInt))
        cd.Parameters.Item("@isadm").Value = 1
        dbNTB.Open()
        If cd.ExecuteScalar = 1 Then
            dbNTB.Close()
            Me.Hide()
            frmMain.sitecode = cbSites.SelectedValue
            frmMain.userid = txtUserId.Text
            frmMain.conString = conString
            frmMain.Show()
        Else
            MessageBox.Show("Feil brukernavn og/eller passord!", "Ingen tilgang", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            txtUserId.Text = ""
            txtPassword.Text = ""
            txtUserId.Focus()
        End If
        dbNTB.Close()
    End Sub

    Private Sub LoginForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'If File.Exists("conString.txt") Then
        '    Dim stream As StreamReader
        '    Dim l As String
        '    stream = File.OpenText("conString.txt")
        '    l = stream.ReadLine()
        '    While l <> Nothing
        '        conString += l
        '        l = stream.ReadLine()
        '    End While
        '    stream.Close()
        'Else
        '    MessageBox.Show("Finner ikke conString.txt. Kan ikke koble opp databasen. Programmet avsluttes!", "Feil", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        '    End
        'End If

        conString = AppSettings("ConnectionString")

        Dim dbNTB As New SqlConnection(conString)
        Dim da As New SqlDataAdapter("SELECT Id, Name FROM SITES order by ID desc", dbNTB)
        Dim ds As New DataSet

        da.Fill(ds, "Sites")
        cbSites.DisplayMember = "Name"
        cbSites.ValueMember = "Id"
        cbSites.DataSource = ds.Tables("Sites")
        cbSites.SelectedValue = "INT"
        txtUserId.Focus()
    End Sub
End Class
