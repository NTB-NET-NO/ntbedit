Imports System.IO
Imports System.Data.SqlClient
'Imports NTBEdit.LoginForm
Imports System.Configuration.ConfigurationSettings

Public Class MainForm
    Inherits System.Windows.Forms.Form

    Private dtSection As DataTable
    Private dtUser As DataTable
    Private userList As ArrayList = New ArrayList

    Private PwdChangedDate As Date = Today
    Private OldPassword As String

    Private currsection, curruser As Integer
    Public userid As String
    Public sitecode As String = "WWW"
    Public conString As String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents sbProg As System.Windows.Forms.StatusBarPanel
    Friend WithEvents sbUser As System.Windows.Forms.StatusBarPanel
    Friend WithEvents sbMain As System.Windows.Forms.StatusBar
    Friend WithEvents sbMisc As System.Windows.Forms.StatusBarPanel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents cbCategories As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents pSection As System.Windows.Forms.Panel
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnCancelSection As System.Windows.Forms.Button
    Friend WithEvents btnSaveSection As System.Windows.Forms.Button
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents btnNewSection As System.Windows.Forms.Button
    Friend WithEvents btnEditSection As System.Windows.Forms.Button
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents lbSections As System.Windows.Forms.ListBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents txtSearchSection As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents web As AxSHDocVw.AxWebBrowser
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnFindSection As System.Windows.Forms.Button
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents AxWebBrowser1 As AxSHDocVw.AxWebBrowser
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents TabControl2 As System.Windows.Forms.TabControl
    Friend WithEvents txtText As System.Windows.Forms.TextBox
    Friend WithEvents txtIngress As System.Windows.Forms.TextBox
    Friend WithEvents txtTitle As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtFooter As System.Windows.Forms.TextBox
    Friend WithEvents txtSection As System.Windows.Forms.TextBox
    Friend WithEvents txtPagecount As System.Windows.Forms.TextBox
    Friend WithEvents txtArchive As System.Windows.Forms.TextBox
    Friend WithEvents txtNumarticles As System.Windows.Forms.TextBox
    Friend WithEvents cbMenues As System.Windows.Forms.ComboBox
    Friend WithEvents chkMenu As System.Windows.Forms.CheckBox
    Friend WithEvents chkSearch As System.Windows.Forms.CheckBox
    Friend WithEvents chkProtected As System.Windows.Forms.CheckBox
    Friend WithEvents lbSubgroup As System.Windows.Forms.ListBox
    Friend WithEvents lbMaingroup As System.Windows.Forms.ListBox
    Friend WithEvents lID As System.Windows.Forms.Label
    Friend WithEvents lLastmodified As System.Windows.Forms.Label
    Friend WithEvents rtxtArticlelistheader As System.Windows.Forms.RichTextBox
    Friend WithEvents tpEgenskaper As System.Windows.Forms.TabPage
    Friend WithEvents tpInnhold As System.Windows.Forms.TabPage
    Friend WithEvents tpHTML As System.Windows.Forms.TabPage
    Friend WithEvents tpBilde As System.Windows.Forms.TabPage
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents txtPictureHeading As System.Windows.Forms.TextBox
    Friend WithEvents txtPictureText As System.Windows.Forms.TextBox
    Friend WithEvents imgSection As System.Windows.Forms.PictureBox
    Friend WithEvents tbSection As System.Windows.Forms.TabPage
    Friend WithEvents tbMenu As System.Windows.Forms.TabPage
    Friend WithEvents tbUser As System.Windows.Forms.TabPage
    Friend WithEvents tbArticle As System.Windows.Forms.TabPage
    Friend WithEvents tbStats As System.Windows.Forms.TabPage
    Friend WithEvents lbMenues As System.Windows.Forms.ListBox
    Friend WithEvents tcStats As System.Windows.Forms.TabControl
    Friend WithEvents tpDownload As System.Windows.Forms.TabPage
    Friend WithEvents tpStats As System.Windows.Forms.TabPage
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents dgDLStats As System.Windows.Forms.DataGrid
    Friend WithEvents cbUsers As System.Windows.Forms.ComboBox
    Friend WithEvents dgStats As System.Windows.Forms.DataGrid
    Friend WithEvents dgArticles As System.Windows.Forms.DataGrid
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents rtxtContent As System.Windows.Forms.RichTextBox
    Friend WithEvents chkHTMLContent As System.Windows.Forms.CheckBox
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents lbUsers As System.Windows.Forms.ListBox
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents lblId As System.Windows.Forms.Label
    Friend WithEvents txtUserId As System.Windows.Forms.TextBox
    Friend WithEvents chkAdmin As System.Windows.Forms.CheckBox
    Friend WithEvents chkInvoice As System.Windows.Forms.CheckBox
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents lbUserMainCat As System.Windows.Forms.ListBox
    Friend WithEvents lbUserMaingroup As System.Windows.Forms.ListBox
    Friend WithEvents btnEditUser As System.Windows.Forms.Button
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents txtIPRange As System.Windows.Forms.TextBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents daSection As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents SqlSelectCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlInsertCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents dbNTB As System.Data.SqlClient.SqlConnection
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents txtPictureHTML As System.Windows.Forms.TextBox
    Friend WithEvents txtTitleGif As System.Windows.Forms.TextBox
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents btnShowDLStats As System.Windows.Forms.Button
    Friend WithEvents btnShowPageStats As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnRemovePicture As System.Windows.Forms.Button
    Friend WithEvents btnRefreshSection As System.Windows.Forms.Button
    Friend WithEvents btnDeleteSection As System.Windows.Forms.Button
    Friend WithEvents btnHTMLSection As System.Windows.Forms.Button
    Friend WithEvents daUser As System.Data.SqlClient.SqlDataAdapter
    Friend WithEvents btnSaveUser As System.Windows.Forms.Button
    Friend WithEvents btnBrowsePicture As System.Windows.Forms.Button
    Friend WithEvents btnNewUser As System.Windows.Forms.Button
    Friend WithEvents btnDeleteUser As System.Windows.Forms.Button
    Friend WithEvents btnRefreshUsers As System.Windows.Forms.Button
    Friend WithEvents pUser As System.Windows.Forms.Panel
    Friend WithEvents btnCancelUser As System.Windows.Forms.Button
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents dtFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtFromDate2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtToDate2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnFindUser As System.Windows.Forms.Button
    Friend WithEvents txtSearchUser As System.Windows.Forms.TextBox
    Friend WithEvents chkActive As System.Windows.Forms.CheckBox
    Friend WithEvents txtNote As System.Windows.Forms.TextBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents SqlSelectCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlUpdateCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents SqlDeleteCommand2 As System.Data.SqlClient.SqlCommand
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents lblLastloggedin As System.Windows.Forms.Label
    Friend WithEvents rbAllUsers As System.Windows.Forms.RadioButton
    Friend WithEvents rbUser As System.Windows.Forms.RadioButton
    Friend WithEvents btnDeleteArticle As System.Windows.Forms.Button
    Friend WithEvents btnShowArticles As System.Windows.Forms.Button
    Friend WithEvents cbInterval As System.Windows.Forms.ComboBox
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents Panel11 As System.Windows.Forms.Panel
    Friend WithEvents Panel12 As System.Windows.Forms.Panel
    Friend WithEvents cbShowUserSG As System.Windows.Forms.ComboBox
    Friend WithEvents allUsers As System.Windows.Forms.RadioButton
    Friend WithEvents onlySG As System.Windows.Forms.RadioButton
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents telefon As System.Windows.Forms.TextBox
    Friend WithEvents adresse As System.Windows.Forms.TextBox
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents lblCreated As System.Windows.Forms.Label
    Friend WithEvents createdBy As System.Windows.Forms.TextBox
    Friend WithEvents SqlCommand1 As System.Data.SqlClient.SqlCommand
    Friend WithEvents chkIPPwd As System.Windows.Forms.CheckBox
    Friend WithEvents tpUserStats As System.Windows.Forms.TabPage
    Friend WithEvents Panel13 As System.Windows.Forms.Panel
    Friend WithEvents Panel14 As System.Windows.Forms.Panel
    Friend WithEvents btnShowUserStats As System.Windows.Forms.Button
    Friend WithEvents rbUsUser As System.Windows.Forms.RadioButton
    Friend WithEvents rbUsAll As System.Windows.Forms.RadioButton
    Friend WithEvents dtToDate3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtFromDate3 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents cbUsUser As System.Windows.Forms.ComboBox
    Friend WithEvents dgUserStats As System.Windows.Forms.DataGrid
    Friend WithEvents ExpireDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Expire As System.Windows.Forms.CheckBox
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents interval As System.Windows.Forms.TextBox

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(MainForm))
        Me.sbMain = New System.Windows.Forms.StatusBar
        Me.sbProg = New System.Windows.Forms.StatusBarPanel
        Me.sbUser = New System.Windows.Forms.StatusBarPanel
        Me.sbMisc = New System.Windows.Forms.StatusBarPanel
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.tbSection = New System.Windows.Forms.TabPage
        Me.Panel6 = New System.Windows.Forms.Panel
        Me.lbSections = New System.Windows.Forms.ListBox
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.txtSearchSection = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.btnFindSection = New System.Windows.Forms.Button
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.btnRefreshSection = New System.Windows.Forms.Button
        Me.btnDeleteSection = New System.Windows.Forms.Button
        Me.btnNewSection = New System.Windows.Forms.Button
        Me.btnEditSection = New System.Windows.Forms.Button
        Me.pSection = New System.Windows.Forms.Panel
        Me.TabControl2 = New System.Windows.Forms.TabControl
        Me.tpEgenskaper = New System.Windows.Forms.TabPage
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lbSubgroup = New System.Windows.Forms.ListBox
        Me.lbMaingroup = New System.Windows.Forms.ListBox
        Me.txtPagecount = New System.Windows.Forms.TextBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.txtArchive = New System.Windows.Forms.TextBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.txtNumarticles = New System.Windows.Forms.TextBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.rtxtArticlelistheader = New System.Windows.Forms.RichTextBox
        Me.Label22 = New System.Windows.Forms.Label
        Me.chkHTMLContent = New System.Windows.Forms.CheckBox
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.Label28 = New System.Windows.Forms.Label
        Me.lID = New System.Windows.Forms.Label
        Me.lLastmodified = New System.Windows.Forms.Label
        Me.txtSection = New System.Windows.Forms.TextBox
        Me.cbMenues = New System.Windows.Forms.ComboBox
        Me.chkMenu = New System.Windows.Forms.CheckBox
        Me.chkSearch = New System.Windows.Forms.CheckBox
        Me.chkProtected = New System.Windows.Forms.CheckBox
        Me.tpInnhold = New System.Windows.Forms.TabPage
        Me.txtTitleGif = New System.Windows.Forms.TextBox
        Me.Label52 = New System.Windows.Forms.Label
        Me.txtFooter = New System.Windows.Forms.TextBox
        Me.Label36 = New System.Windows.Forms.Label
        Me.txtText = New System.Windows.Forms.TextBox
        Me.Label31 = New System.Windows.Forms.Label
        Me.txtIngress = New System.Windows.Forms.TextBox
        Me.Label32 = New System.Windows.Forms.Label
        Me.txtTitle = New System.Windows.Forms.TextBox
        Me.Label33 = New System.Windows.Forms.Label
        Me.tpBilde = New System.Windows.Forms.TabPage
        Me.btnRemovePicture = New System.Windows.Forms.Button
        Me.txtPictureHTML = New System.Windows.Forms.TextBox
        Me.Label45 = New System.Windows.Forms.Label
        Me.btnBrowsePicture = New System.Windows.Forms.Button
        Me.txtPictureText = New System.Windows.Forms.TextBox
        Me.Label37 = New System.Windows.Forms.Label
        Me.imgSection = New System.Windows.Forms.PictureBox
        Me.Label30 = New System.Windows.Forms.Label
        Me.Label29 = New System.Windows.Forms.Label
        Me.txtPictureHeading = New System.Windows.Forms.TextBox
        Me.tpHTML = New System.Windows.Forms.TabPage
        Me.AxWebBrowser1 = New AxSHDocVw.AxWebBrowser
        Me.Label34 = New System.Windows.Forms.Label
        Me.rtxtContent = New System.Windows.Forms.RichTextBox
        Me.Label35 = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.btnHTMLSection = New System.Windows.Forms.Button
        Me.btnCancelSection = New System.Windows.Forms.Button
        Me.btnSaveSection = New System.Windows.Forms.Button
        Me.tbStats = New System.Windows.Forms.TabPage
        Me.tcStats = New System.Windows.Forms.TabControl
        Me.tpDownload = New System.Windows.Forms.TabPage
        Me.dgDLStats = New System.Windows.Forms.DataGrid
        Me.Panel7 = New System.Windows.Forms.Panel
        Me.Panel11 = New System.Windows.Forms.Panel
        Me.btnShowDLStats = New System.Windows.Forms.Button
        Me.rbUser = New System.Windows.Forms.RadioButton
        Me.rbAllUsers = New System.Windows.Forms.RadioButton
        Me.dtToDate = New System.Windows.Forms.DateTimePicker
        Me.dtFromDate = New System.Windows.Forms.DateTimePicker
        Me.Label39 = New System.Windows.Forms.Label
        Me.Label38 = New System.Windows.Forms.Label
        Me.cbUsers = New System.Windows.Forms.ComboBox
        Me.tpStats = New System.Windows.Forms.TabPage
        Me.dgStats = New System.Windows.Forms.DataGrid
        Me.Panel8 = New System.Windows.Forms.Panel
        Me.Panel12 = New System.Windows.Forms.Panel
        Me.btnShowPageStats = New System.Windows.Forms.Button
        Me.dtToDate2 = New System.Windows.Forms.DateTimePicker
        Me.dtFromDate2 = New System.Windows.Forms.DateTimePicker
        Me.Label40 = New System.Windows.Forms.Label
        Me.Label41 = New System.Windows.Forms.Label
        Me.tpUserStats = New System.Windows.Forms.TabPage
        Me.dgUserStats = New System.Windows.Forms.DataGrid
        Me.Panel13 = New System.Windows.Forms.Panel
        Me.Panel14 = New System.Windows.Forms.Panel
        Me.btnShowUserStats = New System.Windows.Forms.Button
        Me.rbUsUser = New System.Windows.Forms.RadioButton
        Me.rbUsAll = New System.Windows.Forms.RadioButton
        Me.dtToDate3 = New System.Windows.Forms.DateTimePicker
        Me.dtFromDate3 = New System.Windows.Forms.DateTimePicker
        Me.Label61 = New System.Windows.Forms.Label
        Me.Label62 = New System.Windows.Forms.Label
        Me.cbUsUser = New System.Windows.Forms.ComboBox
        Me.tbUser = New System.Windows.Forms.TabPage
        Me.Panel9 = New System.Windows.Forms.Panel
        Me.btnRefreshUsers = New System.Windows.Forms.Button
        Me.btnDeleteUser = New System.Windows.Forms.Button
        Me.btnNewUser = New System.Windows.Forms.Button
        Me.btnEditUser = New System.Windows.Forms.Button
        Me.pUser = New System.Windows.Forms.Panel
        Me.interval = New System.Windows.Forms.TextBox
        Me.Label63 = New System.Windows.Forms.Label
        Me.Expire = New System.Windows.Forms.CheckBox
        Me.ExpireDate = New System.Windows.Forms.DateTimePicker
        Me.chkIPPwd = New System.Windows.Forms.CheckBox
        Me.Label60 = New System.Windows.Forms.Label
        Me.lblCreated = New System.Windows.Forms.Label
        Me.Label59 = New System.Windows.Forms.Label
        Me.createdBy = New System.Windows.Forms.TextBox
        Me.adresse = New System.Windows.Forms.TextBox
        Me.Label58 = New System.Windows.Forms.Label
        Me.Label57 = New System.Windows.Forms.Label
        Me.telefon = New System.Windows.Forms.TextBox
        Me.Label56 = New System.Windows.Forms.Label
        Me.Label53 = New System.Windows.Forms.Label
        Me.Label55 = New System.Windows.Forms.Label
        Me.lblLastloggedin = New System.Windows.Forms.Label
        Me.txtNote = New System.Windows.Forms.TextBox
        Me.Label54 = New System.Windows.Forms.Label
        Me.chkActive = New System.Windows.Forms.CheckBox
        Me.btnCancelUser = New System.Windows.Forms.Button
        Me.txtIPRange = New System.Windows.Forms.TextBox
        Me.Label44 = New System.Windows.Forms.Label
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label43 = New System.Windows.Forms.Label
        Me.Label48 = New System.Windows.Forms.Label
        Me.txtEmail = New System.Windows.Forms.TextBox
        Me.Label42 = New System.Windows.Forms.Label
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.Label46 = New System.Windows.Forms.Label
        Me.Label47 = New System.Windows.Forms.Label
        Me.Label49 = New System.Windows.Forms.Label
        Me.Label50 = New System.Windows.Forms.Label
        Me.Label51 = New System.Windows.Forms.Label
        Me.lblId = New System.Windows.Forms.Label
        Me.txtUserId = New System.Windows.Forms.TextBox
        Me.chkAdmin = New System.Windows.Forms.CheckBox
        Me.chkInvoice = New System.Windows.Forms.CheckBox
        Me.lbUserMainCat = New System.Windows.Forms.ListBox
        Me.lbUserMaingroup = New System.Windows.Forms.ListBox
        Me.btnSaveUser = New System.Windows.Forms.Button
        Me.lbUsers = New System.Windows.Forms.ListBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.onlySG = New System.Windows.Forms.RadioButton
        Me.allUsers = New System.Windows.Forms.RadioButton
        Me.cbShowUserSG = New System.Windows.Forms.ComboBox
        Me.txtSearchUser = New System.Windows.Forms.TextBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.btnFindUser = New System.Windows.Forms.Button
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.tbArticle = New System.Windows.Forms.TabPage
        Me.dgArticles = New System.Windows.Forms.DataGrid
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.Panel10 = New System.Windows.Forms.Panel
        Me.btnDeleteArticle = New System.Windows.Forms.Button
        Me.btnShowArticles = New System.Windows.Forms.Button
        Me.cbInterval = New System.Windows.Forms.ComboBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.Label6 = New System.Windows.Forms.Label
        Me.cbCategories = New System.Windows.Forms.ComboBox
        Me.tbMenu = New System.Windows.Forms.TabPage
        Me.lbMenues = New System.Windows.Forms.ListBox
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Button11 = New System.Windows.Forms.Button
        Me.Button14 = New System.Windows.Forms.Button
        Me.Button15 = New System.Windows.Forms.Button
        Me.Button16 = New System.Windows.Forms.Button
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.web = New AxSHDocVw.AxWebBrowser
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.daSection = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand1 = New System.Data.SqlClient.SqlCommand
        Me.dbNTB = New System.Data.SqlClient.SqlConnection
        Me.SqlInsertCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand1 = New System.Data.SqlClient.SqlCommand
        Me.daUser = New System.Data.SqlClient.SqlDataAdapter
        Me.SqlDeleteCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlCommand1 = New System.Data.SqlClient.SqlCommand
        Me.SqlSelectCommand2 = New System.Data.SqlClient.SqlCommand
        Me.SqlUpdateCommand2 = New System.Data.SqlClient.SqlCommand
        CType(Me.sbProg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sbUser, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sbMisc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.tbSection.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.pSection.SuspendLayout()
        Me.TabControl2.SuspendLayout()
        Me.tpEgenskaper.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.tpInnhold.SuspendLayout()
        Me.tpBilde.SuspendLayout()
        Me.tpHTML.SuspendLayout()
        CType(Me.AxWebBrowser1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.tbStats.SuspendLayout()
        Me.tcStats.SuspendLayout()
        Me.tpDownload.SuspendLayout()
        CType(Me.dgDLStats, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel7.SuspendLayout()
        Me.Panel11.SuspendLayout()
        Me.tpStats.SuspendLayout()
        CType(Me.dgStats, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel8.SuspendLayout()
        Me.Panel12.SuspendLayout()
        Me.tpUserStats.SuspendLayout()
        CType(Me.dgUserStats, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel13.SuspendLayout()
        Me.Panel14.SuspendLayout()
        Me.tbUser.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.pUser.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.tbArticle.SuspendLayout()
        CType(Me.dgArticles, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.Panel10.SuspendLayout()
        Me.tbMenu.SuspendLayout()
        CType(Me.web, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'sbMain
        '
        Me.sbMain.Location = New System.Drawing.Point(0, 608)
        Me.sbMain.Name = "sbMain"
        Me.sbMain.Panels.AddRange(New System.Windows.Forms.StatusBarPanel() {Me.sbProg, Me.sbUser, Me.sbMisc})
        Me.sbMain.ShowPanels = True
        Me.sbMain.Size = New System.Drawing.Size(848, 22)
        Me.sbMain.TabIndex = 0
        '
        'sbProg
        '
        Me.sbProg.Text = "NTBEdit v. 0.1b"
        '
        'sbUser
        '
        Me.sbUser.Text = "Username:"
        Me.sbUser.Width = 200
        '
        'sbMisc
        '
        Me.sbMisc.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring
        Me.sbMisc.Width = 532
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.tbSection)
        Me.TabControl1.Controls.Add(Me.tbStats)
        Me.TabControl1.Controls.Add(Me.tbUser)
        Me.TabControl1.Controls.Add(Me.tbArticle)
        Me.TabControl1.Controls.Add(Me.tbMenu)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(848, 608)
        Me.TabControl1.TabIndex = 1
        '
        'tbSection
        '
        Me.tbSection.Controls.Add(Me.Panel6)
        Me.tbSection.Controls.Add(Me.Panel4)
        Me.tbSection.Controls.Add(Me.pSection)
        Me.tbSection.Location = New System.Drawing.Point(4, 22)
        Me.tbSection.Name = "tbSection"
        Me.tbSection.Size = New System.Drawing.Size(840, 582)
        Me.tbSection.TabIndex = 0
        Me.tbSection.Text = "Seksjoner"
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.lbSections)
        Me.Panel6.Controls.Add(Me.Panel5)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel6.Location = New System.Drawing.Point(0, 0)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(256, 510)
        Me.Panel6.TabIndex = 17
        '
        'lbSections
        '
        Me.lbSections.Location = New System.Drawing.Point(8, 40)
        Me.lbSections.Name = "lbSections"
        Me.lbSections.Size = New System.Drawing.Size(240, 459)
        Me.lbSections.TabIndex = 13
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.txtSearchSection)
        Me.Panel5.Controls.Add(Me.Label3)
        Me.Panel5.Controls.Add(Me.btnFindSection)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel5.Location = New System.Drawing.Point(0, 0)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(256, 40)
        Me.Panel5.TabIndex = 18
        '
        'txtSearchSection
        '
        Me.txtSearchSection.Location = New System.Drawing.Point(64, 8)
        Me.txtSearchSection.Name = "txtSearchSection"
        Me.txtSearchSection.Size = New System.Drawing.Size(88, 20)
        Me.txtSearchSection.TabIndex = 12
        Me.txtSearchSection.Text = ""
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(8, 8)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 23)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "S�k:"
        '
        'btnFindSection
        '
        Me.btnFindSection.Location = New System.Drawing.Point(168, 8)
        Me.btnFindSection.Name = "btnFindSection"
        Me.btnFindSection.TabIndex = 13
        Me.btnFindSection.Text = "S�k"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.btnRefreshSection)
        Me.Panel4.Controls.Add(Me.btnDeleteSection)
        Me.Panel4.Controls.Add(Me.btnNewSection)
        Me.Panel4.Controls.Add(Me.btnEditSection)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel4.Location = New System.Drawing.Point(0, 510)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(256, 72)
        Me.Panel4.TabIndex = 14
        '
        'btnRefreshSection
        '
        Me.btnRefreshSection.Location = New System.Drawing.Point(8, 32)
        Me.btnRefreshSection.Name = "btnRefreshSection"
        Me.btnRefreshSection.TabIndex = 19
        Me.btnRefreshSection.Text = "Oppdater"
        '
        'btnDeleteSection
        '
        Me.btnDeleteSection.Location = New System.Drawing.Point(168, 0)
        Me.btnDeleteSection.Name = "btnDeleteSection"
        Me.btnDeleteSection.TabIndex = 16
        Me.btnDeleteSection.Text = "Slett"
        '
        'btnNewSection
        '
        Me.btnNewSection.Location = New System.Drawing.Point(88, 0)
        Me.btnNewSection.Name = "btnNewSection"
        Me.btnNewSection.TabIndex = 15
        Me.btnNewSection.Text = "Ny"
        '
        'btnEditSection
        '
        Me.btnEditSection.Location = New System.Drawing.Point(8, 0)
        Me.btnEditSection.Name = "btnEditSection"
        Me.btnEditSection.TabIndex = 14
        Me.btnEditSection.Text = "Rediger"
        '
        'pSection
        '
        Me.pSection.Controls.Add(Me.TabControl2)
        Me.pSection.Controls.Add(Me.Panel2)
        Me.pSection.Dock = System.Windows.Forms.DockStyle.Right
        Me.pSection.Location = New System.Drawing.Point(256, 0)
        Me.pSection.Name = "pSection"
        Me.pSection.Size = New System.Drawing.Size(584, 582)
        Me.pSection.TabIndex = 4
        Me.pSection.Visible = False
        '
        'TabControl2
        '
        Me.TabControl2.Controls.Add(Me.tpEgenskaper)
        Me.TabControl2.Controls.Add(Me.tpInnhold)
        Me.TabControl2.Controls.Add(Me.tpBilde)
        Me.TabControl2.Controls.Add(Me.tpHTML)
        Me.TabControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl2.ItemSize = New System.Drawing.Size(69, 18)
        Me.TabControl2.Location = New System.Drawing.Point(0, 0)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(584, 542)
        Me.TabControl2.TabIndex = 28
        '
        'tpEgenskaper
        '
        Me.tpEgenskaper.Controls.Add(Me.GroupBox1)
        Me.tpEgenskaper.Controls.Add(Me.chkHTMLContent)
        Me.tpEgenskaper.Controls.Add(Me.Label26)
        Me.tpEgenskaper.Controls.Add(Me.Label27)
        Me.tpEgenskaper.Controls.Add(Me.Label28)
        Me.tpEgenskaper.Controls.Add(Me.lID)
        Me.tpEgenskaper.Controls.Add(Me.lLastmodified)
        Me.tpEgenskaper.Controls.Add(Me.txtSection)
        Me.tpEgenskaper.Controls.Add(Me.cbMenues)
        Me.tpEgenskaper.Controls.Add(Me.chkMenu)
        Me.tpEgenskaper.Controls.Add(Me.chkSearch)
        Me.tpEgenskaper.Controls.Add(Me.chkProtected)
        Me.tpEgenskaper.Location = New System.Drawing.Point(4, 22)
        Me.tpEgenskaper.Name = "tpEgenskaper"
        Me.tpEgenskaper.Size = New System.Drawing.Size(576, 516)
        Me.tpEgenskaper.TabIndex = 0
        Me.tpEgenskaper.Text = "Egenskaper"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lbSubgroup)
        Me.GroupBox1.Controls.Add(Me.lbMaingroup)
        Me.GroupBox1.Controls.Add(Me.txtPagecount)
        Me.GroupBox1.Controls.Add(Me.Label21)
        Me.GroupBox1.Controls.Add(Me.Label25)
        Me.GroupBox1.Controls.Add(Me.txtArchive)
        Me.GroupBox1.Controls.Add(Me.Label23)
        Me.GroupBox1.Controls.Add(Me.Label24)
        Me.GroupBox1.Controls.Add(Me.txtNumarticles)
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.rtxtArticlelistheader)
        Me.GroupBox1.Controls.Add(Me.Label22)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 112)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(560, 232)
        Me.GroupBox1.TabIndex = 30
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Seksjoner med artikler"
        '
        'lbSubgroup
        '
        Me.lbSubgroup.Enabled = False
        Me.lbSubgroup.Location = New System.Drawing.Point(88, 128)
        Me.lbSubgroup.Name = "lbSubgroup"
        Me.lbSubgroup.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.lbSubgroup.Size = New System.Drawing.Size(144, 95)
        Me.lbSubgroup.TabIndex = 1
        '
        'lbMaingroup
        '
        Me.lbMaingroup.Enabled = False
        Me.lbMaingroup.Location = New System.Drawing.Point(88, 24)
        Me.lbMaingroup.Name = "lbMaingroup"
        Me.lbMaingroup.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.lbMaingroup.Size = New System.Drawing.Size(144, 95)
        Me.lbMaingroup.TabIndex = 0
        '
        'txtPagecount
        '
        Me.txtPagecount.Enabled = False
        Me.txtPagecount.Location = New System.Drawing.Point(320, 192)
        Me.txtPagecount.Name = "txtPagecount"
        Me.txtPagecount.Size = New System.Drawing.Size(48, 20)
        Me.txtPagecount.TabIndex = 8
        Me.txtPagecount.Text = ""
        '
        'Label21
        '
        Me.Label21.Enabled = False
        Me.Label21.Location = New System.Drawing.Point(240, 192)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(88, 23)
        Me.Label21.TabIndex = 19
        Me.Label21.Text = "Artikler pr. side:"
        '
        'Label25
        '
        Me.Label25.Location = New System.Drawing.Point(8, 24)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(88, 23)
        Me.Label25.TabIndex = 15
        Me.Label25.Text = "Stoffgruppe:"
        '
        'txtArchive
        '
        Me.txtArchive.Location = New System.Drawing.Point(320, 160)
        Me.txtArchive.Name = "txtArchive"
        Me.txtArchive.Size = New System.Drawing.Size(48, 20)
        Me.txtArchive.TabIndex = 7
        Me.txtArchive.Text = ""
        '
        'Label23
        '
        Me.Label23.Location = New System.Drawing.Point(240, 128)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(80, 23)
        Me.Label23.TabIndex = 17
        Me.Label23.Text = "Antall dager:"
        '
        'Label24
        '
        Me.Label24.Location = New System.Drawing.Point(8, 128)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(88, 23)
        Me.Label24.TabIndex = 16
        Me.Label24.Text = "Undergruppe:"
        '
        'txtNumarticles
        '
        Me.txtNumarticles.Location = New System.Drawing.Point(320, 128)
        Me.txtNumarticles.Name = "txtNumarticles"
        Me.txtNumarticles.Size = New System.Drawing.Size(48, 20)
        Me.txtNumarticles.TabIndex = 6
        Me.txtNumarticles.Text = ""
        '
        'Label20
        '
        Me.Label20.Location = New System.Drawing.Point(240, 24)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(80, 16)
        Me.Label20.TabIndex = 28
        Me.Label20.Text = "Art.liste hode:"
        '
        'rtxtArticlelistheader
        '
        Me.rtxtArticlelistheader.AcceptsTab = True
        Me.rtxtArticlelistheader.Location = New System.Drawing.Point(320, 24)
        Me.rtxtArticlelistheader.Name = "rtxtArticlelistheader"
        Me.rtxtArticlelistheader.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
        Me.rtxtArticlelistheader.Size = New System.Drawing.Size(232, 96)
        Me.rtxtArticlelistheader.TabIndex = 27
        Me.rtxtArticlelistheader.Text = ""
        '
        'Label22
        '
        Me.Label22.Location = New System.Drawing.Point(240, 160)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(72, 23)
        Me.Label22.TabIndex = 18
        Me.Label22.Text = "Dager, arkiv:"
        '
        'chkHTMLContent
        '
        Me.chkHTMLContent.Location = New System.Drawing.Point(256, 80)
        Me.chkHTMLContent.Name = "chkHTMLContent"
        Me.chkHTMLContent.TabIndex = 29
        Me.chkHTMLContent.Text = "HTML innhold"
        '
        'Label26
        '
        Me.Label26.Enabled = False
        Me.Label26.Location = New System.Drawing.Point(8, 64)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(88, 23)
        Me.Label26.TabIndex = 14
        Me.Label26.Text = "Meny:"
        '
        'Label27
        '
        Me.Label27.Location = New System.Drawing.Point(8, 32)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(88, 23)
        Me.Label27.TabIndex = 13
        Me.Label27.Text = "Seksjon:"
        '
        'Label28
        '
        Me.Label28.Location = New System.Drawing.Point(8, 8)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(88, 23)
        Me.Label28.TabIndex = 12
        Me.Label28.Text = "Sist endret:"
        '
        'lID
        '
        Me.lID.Location = New System.Drawing.Point(232, 32)
        Me.lID.Name = "lID"
        Me.lID.Size = New System.Drawing.Size(24, 23)
        Me.lID.TabIndex = 11
        '
        'lLastmodified
        '
        Me.lLastmodified.Location = New System.Drawing.Point(96, 8)
        Me.lLastmodified.Name = "lLastmodified"
        Me.lLastmodified.Size = New System.Drawing.Size(128, 23)
        Me.lLastmodified.TabIndex = 10
        '
        'txtSection
        '
        Me.txtSection.Location = New System.Drawing.Point(96, 32)
        Me.txtSection.Name = "txtSection"
        Me.txtSection.Size = New System.Drawing.Size(128, 20)
        Me.txtSection.TabIndex = 9
        Me.txtSection.Text = ""
        '
        'cbMenues
        '
        Me.cbMenues.Enabled = False
        Me.cbMenues.ItemHeight = 13
        Me.cbMenues.Location = New System.Drawing.Point(96, 64)
        Me.cbMenues.Name = "cbMenues"
        Me.cbMenues.Size = New System.Drawing.Size(128, 21)
        Me.cbMenues.TabIndex = 5
        '
        'chkMenu
        '
        Me.chkMenu.Enabled = False
        Me.chkMenu.Location = New System.Drawing.Point(256, 56)
        Me.chkMenu.Name = "chkMenu"
        Me.chkMenu.TabIndex = 4
        Me.chkMenu.Text = "Vis meny"
        '
        'chkSearch
        '
        Me.chkSearch.Location = New System.Drawing.Point(256, 8)
        Me.chkSearch.Name = "chkSearch"
        Me.chkSearch.TabIndex = 3
        Me.chkSearch.Text = "Vis s�k"
        '
        'chkProtected
        '
        Me.chkProtected.Location = New System.Drawing.Point(256, 32)
        Me.chkProtected.Name = "chkProtected"
        Me.chkProtected.Size = New System.Drawing.Size(120, 24)
        Me.chkProtected.TabIndex = 2
        Me.chkProtected.Text = "Passord beskyttet"
        '
        'tpInnhold
        '
        Me.tpInnhold.Controls.Add(Me.txtTitleGif)
        Me.tpInnhold.Controls.Add(Me.Label52)
        Me.tpInnhold.Controls.Add(Me.txtFooter)
        Me.tpInnhold.Controls.Add(Me.Label36)
        Me.tpInnhold.Controls.Add(Me.txtText)
        Me.tpInnhold.Controls.Add(Me.Label31)
        Me.tpInnhold.Controls.Add(Me.txtIngress)
        Me.tpInnhold.Controls.Add(Me.Label32)
        Me.tpInnhold.Controls.Add(Me.txtTitle)
        Me.tpInnhold.Controls.Add(Me.Label33)
        Me.tpInnhold.Location = New System.Drawing.Point(4, 22)
        Me.tpInnhold.Name = "tpInnhold"
        Me.tpInnhold.Size = New System.Drawing.Size(576, 516)
        Me.tpInnhold.TabIndex = 2
        Me.tpInnhold.Text = "Innhold"
        Me.tpInnhold.Visible = False
        '
        'txtTitleGif
        '
        Me.txtTitleGif.Location = New System.Drawing.Point(64, 40)
        Me.txtTitleGif.Name = "txtTitleGif"
        Me.txtTitleGif.Size = New System.Drawing.Size(496, 20)
        Me.txtTitleGif.TabIndex = 2
        Me.txtTitleGif.Text = ""
        '
        'Label52
        '
        Me.Label52.Location = New System.Drawing.Point(8, 40)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(56, 23)
        Me.Label52.TabIndex = 8
        Me.Label52.Text = "Tittel-gif:"
        '
        'txtFooter
        '
        Me.txtFooter.AcceptsReturn = True
        Me.txtFooter.Location = New System.Drawing.Point(64, 448)
        Me.txtFooter.Multiline = True
        Me.txtFooter.Name = "txtFooter"
        Me.txtFooter.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtFooter.Size = New System.Drawing.Size(496, 56)
        Me.txtFooter.TabIndex = 6
        Me.txtFooter.Text = ""
        '
        'Label36
        '
        Me.Label36.Location = New System.Drawing.Point(8, 448)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(64, 23)
        Me.Label36.TabIndex = 6
        Me.Label36.Text = "Bunntekst:"
        '
        'txtText
        '
        Me.txtText.Location = New System.Drawing.Point(64, 176)
        Me.txtText.Multiline = True
        Me.txtText.Name = "txtText"
        Me.txtText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtText.Size = New System.Drawing.Size(496, 264)
        Me.txtText.TabIndex = 5
        Me.txtText.Text = ""
        '
        'Label31
        '
        Me.Label31.Location = New System.Drawing.Point(8, 176)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(56, 23)
        Me.Label31.TabIndex = 4
        Me.Label31.Text = "Tekst:"
        '
        'txtIngress
        '
        Me.txtIngress.AcceptsReturn = True
        Me.txtIngress.Location = New System.Drawing.Point(64, 72)
        Me.txtIngress.Multiline = True
        Me.txtIngress.Name = "txtIngress"
        Me.txtIngress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtIngress.Size = New System.Drawing.Size(496, 96)
        Me.txtIngress.TabIndex = 3
        Me.txtIngress.Text = ""
        '
        'Label32
        '
        Me.Label32.Location = New System.Drawing.Point(8, 72)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(48, 23)
        Me.Label32.TabIndex = 2
        Me.Label32.Text = "Ingress:"
        '
        'txtTitle
        '
        Me.txtTitle.Location = New System.Drawing.Point(64, 8)
        Me.txtTitle.Name = "txtTitle"
        Me.txtTitle.Size = New System.Drawing.Size(496, 20)
        Me.txtTitle.TabIndex = 1
        Me.txtTitle.Text = ""
        '
        'Label33
        '
        Me.Label33.Location = New System.Drawing.Point(8, 8)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(48, 23)
        Me.Label33.TabIndex = 0
        Me.Label33.Text = "Tittel:"
        '
        'tpBilde
        '
        Me.tpBilde.Controls.Add(Me.btnRemovePicture)
        Me.tpBilde.Controls.Add(Me.txtPictureHTML)
        Me.tpBilde.Controls.Add(Me.Label45)
        Me.tpBilde.Controls.Add(Me.btnBrowsePicture)
        Me.tpBilde.Controls.Add(Me.txtPictureText)
        Me.tpBilde.Controls.Add(Me.Label37)
        Me.tpBilde.Controls.Add(Me.imgSection)
        Me.tpBilde.Controls.Add(Me.Label30)
        Me.tpBilde.Controls.Add(Me.Label29)
        Me.tpBilde.Controls.Add(Me.txtPictureHeading)
        Me.tpBilde.Location = New System.Drawing.Point(4, 22)
        Me.tpBilde.Name = "tpBilde"
        Me.tpBilde.Size = New System.Drawing.Size(576, 516)
        Me.tpBilde.TabIndex = 3
        Me.tpBilde.Text = "Bilde"
        '
        'btnRemovePicture
        '
        Me.btnRemovePicture.Location = New System.Drawing.Point(464, 264)
        Me.btnRemovePicture.Name = "btnRemovePicture"
        Me.btnRemovePicture.Size = New System.Drawing.Size(96, 23)
        Me.btnRemovePicture.TabIndex = 22
        Me.btnRemovePicture.Text = "Fjern bilde"
        '
        'txtPictureHTML
        '
        Me.txtPictureHTML.AcceptsTab = True
        Me.txtPictureHTML.Location = New System.Drawing.Point(64, 408)
        Me.txtPictureHTML.Multiline = True
        Me.txtPictureHTML.Name = "txtPictureHTML"
        Me.txtPictureHTML.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtPictureHTML.Size = New System.Drawing.Size(496, 96)
        Me.txtPictureHTML.TabIndex = 21
        Me.txtPictureHTML.Text = ""
        '
        'Label45
        '
        Me.Label45.Location = New System.Drawing.Point(8, 408)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(56, 32)
        Me.Label45.TabIndex = 20
        Me.Label45.Text = "HTML  (for flash):"
        '
        'btnBrowsePicture
        '
        Me.btnBrowsePicture.Location = New System.Drawing.Point(464, 296)
        Me.btnBrowsePicture.Name = "btnBrowsePicture"
        Me.btnBrowsePicture.Size = New System.Drawing.Size(96, 23)
        Me.btnBrowsePicture.TabIndex = 19
        Me.btnBrowsePicture.Text = "Last opp bilde"
        '
        'txtPictureText
        '
        Me.txtPictureText.AcceptsTab = True
        Me.txtPictureText.Location = New System.Drawing.Point(64, 328)
        Me.txtPictureText.Multiline = True
        Me.txtPictureText.Name = "txtPictureText"
        Me.txtPictureText.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtPictureText.Size = New System.Drawing.Size(496, 72)
        Me.txtPictureText.TabIndex = 18
        Me.txtPictureText.Text = ""
        '
        'Label37
        '
        Me.Label37.Location = New System.Drawing.Point(8, 328)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(56, 23)
        Me.Label37.TabIndex = 17
        Me.Label37.Text = "Tekst:"
        '
        'imgSection
        '
        Me.imgSection.Location = New System.Drawing.Point(72, 40)
        Me.imgSection.Name = "imgSection"
        Me.imgSection.Size = New System.Drawing.Size(384, 280)
        Me.imgSection.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.imgSection.TabIndex = 16
        Me.imgSection.TabStop = False
        '
        'Label30
        '
        Me.Label30.Location = New System.Drawing.Point(8, 40)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(56, 23)
        Me.Label30.TabIndex = 15
        Me.Label30.Text = "Bilde:"
        '
        'Label29
        '
        Me.Label29.Location = New System.Drawing.Point(8, 8)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(56, 23)
        Me.Label29.TabIndex = 14
        Me.Label29.Text = "Bildetittel:"
        '
        'txtPictureHeading
        '
        Me.txtPictureHeading.Location = New System.Drawing.Point(64, 8)
        Me.txtPictureHeading.Name = "txtPictureHeading"
        Me.txtPictureHeading.Size = New System.Drawing.Size(496, 20)
        Me.txtPictureHeading.TabIndex = 0
        Me.txtPictureHeading.Text = ""
        '
        'tpHTML
        '
        Me.tpHTML.Controls.Add(Me.AxWebBrowser1)
        Me.tpHTML.Controls.Add(Me.Label34)
        Me.tpHTML.Controls.Add(Me.rtxtContent)
        Me.tpHTML.Controls.Add(Me.Label35)
        Me.tpHTML.Location = New System.Drawing.Point(4, 22)
        Me.tpHTML.Name = "tpHTML"
        Me.tpHTML.Size = New System.Drawing.Size(576, 516)
        Me.tpHTML.TabIndex = 1
        Me.tpHTML.Text = "HTML"
        Me.tpHTML.Visible = False
        '
        'AxWebBrowser1
        '
        Me.AxWebBrowser1.ContainingControl = Me
        Me.AxWebBrowser1.Enabled = True
        Me.AxWebBrowser1.Location = New System.Drawing.Point(128, 72)
        Me.AxWebBrowser1.OcxState = CType(resources.GetObject("AxWebBrowser1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxWebBrowser1.Size = New System.Drawing.Size(544, 344)
        Me.AxWebBrowser1.TabIndex = 28
        Me.AxWebBrowser1.Visible = False
        '
        'Label34
        '
        Me.Label34.Location = New System.Drawing.Point(8, 8)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(72, 16)
        Me.Label34.TabIndex = 27
        Me.Label34.Text = "HTML:"
        '
        'rtxtContent
        '
        Me.rtxtContent.AcceptsTab = True
        Me.rtxtContent.Location = New System.Drawing.Point(8, 24)
        Me.rtxtContent.Name = "rtxtContent"
        Me.rtxtContent.Size = New System.Drawing.Size(560, 480)
        Me.rtxtContent.TabIndex = 7
        Me.rtxtContent.Text = ""
        Me.rtxtContent.WordWrap = False
        '
        'Label35
        '
        Me.Label35.Location = New System.Drawing.Point(-4, -65)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(100, 65)
        Me.Label35.TabIndex = 6
        Me.Label35.Text = "Innhold:"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnHTMLSection)
        Me.Panel2.Controls.Add(Me.btnCancelSection)
        Me.Panel2.Controls.Add(Me.btnSaveSection)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 542)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(584, 40)
        Me.Panel2.TabIndex = 27
        '
        'btnHTMLSection
        '
        Me.btnHTMLSection.Enabled = False
        Me.btnHTMLSection.Location = New System.Drawing.Point(168, 8)
        Me.btnHTMLSection.Name = "btnHTMLSection"
        Me.btnHTMLSection.TabIndex = 5
        Me.btnHTMLSection.Text = "&HTML"
        '
        'btnCancelSection
        '
        Me.btnCancelSection.Location = New System.Drawing.Point(88, 8)
        Me.btnCancelSection.Name = "btnCancelSection"
        Me.btnCancelSection.TabIndex = 3
        Me.btnCancelSection.Text = "Avbryt"
        '
        'btnSaveSection
        '
        Me.btnSaveSection.Location = New System.Drawing.Point(8, 8)
        Me.btnSaveSection.Name = "btnSaveSection"
        Me.btnSaveSection.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnSaveSection.TabIndex = 2
        Me.btnSaveSection.Text = "Lagre"
        '
        'tbStats
        '
        Me.tbStats.Controls.Add(Me.tcStats)
        Me.tbStats.Location = New System.Drawing.Point(4, 22)
        Me.tbStats.Name = "tbStats"
        Me.tbStats.Size = New System.Drawing.Size(840, 582)
        Me.tbStats.TabIndex = 6
        Me.tbStats.Text = "Statistikk"
        '
        'tcStats
        '
        Me.tcStats.Controls.Add(Me.tpDownload)
        Me.tcStats.Controls.Add(Me.tpStats)
        Me.tcStats.Controls.Add(Me.tpUserStats)
        Me.tcStats.Location = New System.Drawing.Point(0, 0)
        Me.tcStats.Name = "tcStats"
        Me.tcStats.SelectedIndex = 0
        Me.tcStats.Size = New System.Drawing.Size(840, 582)
        Me.tcStats.TabIndex = 0
        '
        'tpDownload
        '
        Me.tpDownload.Controls.Add(Me.dgDLStats)
        Me.tpDownload.Controls.Add(Me.Panel7)
        Me.tpDownload.Location = New System.Drawing.Point(4, 22)
        Me.tpDownload.Name = "tpDownload"
        Me.tpDownload.Size = New System.Drawing.Size(832, 556)
        Me.tpDownload.TabIndex = 0
        Me.tpDownload.Text = "Nedlastning"
        '
        'dgDLStats
        '
        Me.dgDLStats.AlternatingBackColor = System.Drawing.Color.WhiteSmoke
        Me.dgDLStats.BackColor = System.Drawing.Color.Gainsboro
        Me.dgDLStats.BackgroundColor = System.Drawing.Color.DarkGray
        Me.dgDLStats.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.dgDLStats.CaptionBackColor = System.Drawing.Color.DarkKhaki
        Me.dgDLStats.CaptionFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.dgDLStats.CaptionForeColor = System.Drawing.Color.Black
        Me.dgDLStats.DataMember = ""
        Me.dgDLStats.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgDLStats.FlatMode = True
        Me.dgDLStats.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.dgDLStats.ForeColor = System.Drawing.Color.Black
        Me.dgDLStats.GridLineColor = System.Drawing.Color.Silver
        Me.dgDLStats.HeaderBackColor = System.Drawing.Color.Black
        Me.dgDLStats.HeaderFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.dgDLStats.HeaderForeColor = System.Drawing.Color.White
        Me.dgDLStats.LinkColor = System.Drawing.Color.DarkSlateBlue
        Me.dgDLStats.Location = New System.Drawing.Point(0, 40)
        Me.dgDLStats.Name = "dgDLStats"
        Me.dgDLStats.ParentRowsBackColor = System.Drawing.Color.LightGray
        Me.dgDLStats.ParentRowsForeColor = System.Drawing.Color.Black
        Me.dgDLStats.PreferredColumnWidth = 125
        Me.dgDLStats.SelectionBackColor = System.Drawing.Color.Firebrick
        Me.dgDLStats.SelectionForeColor = System.Drawing.Color.White
        Me.dgDLStats.Size = New System.Drawing.Size(832, 516)
        Me.dgDLStats.TabIndex = 1
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.Panel11)
        Me.Panel7.Controls.Add(Me.rbUser)
        Me.Panel7.Controls.Add(Me.rbAllUsers)
        Me.Panel7.Controls.Add(Me.dtToDate)
        Me.Panel7.Controls.Add(Me.dtFromDate)
        Me.Panel7.Controls.Add(Me.Label39)
        Me.Panel7.Controls.Add(Me.Label38)
        Me.Panel7.Controls.Add(Me.cbUsers)
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel7.Location = New System.Drawing.Point(0, 0)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(832, 40)
        Me.Panel7.TabIndex = 0
        '
        'Panel11
        '
        Me.Panel11.Controls.Add(Me.btnShowDLStats)
        Me.Panel11.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel11.Location = New System.Drawing.Point(744, 0)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(88, 40)
        Me.Panel11.TabIndex = 12
        '
        'btnShowDLStats
        '
        Me.btnShowDLStats.Location = New System.Drawing.Point(8, 8)
        Me.btnShowDLStats.Name = "btnShowDLStats"
        Me.btnShowDLStats.TabIndex = 0
        Me.btnShowDLStats.Text = "Vis"
        '
        'rbUser
        '
        Me.rbUser.Location = New System.Drawing.Point(448, 8)
        Me.rbUser.Name = "rbUser"
        Me.rbUser.Size = New System.Drawing.Size(72, 24)
        Me.rbUser.TabIndex = 11
        Me.rbUser.Text = "Bruker:"
        '
        'rbAllUsers
        '
        Me.rbAllUsers.Checked = True
        Me.rbAllUsers.Location = New System.Drawing.Point(392, 8)
        Me.rbAllUsers.Name = "rbAllUsers"
        Me.rbAllUsers.Size = New System.Drawing.Size(56, 24)
        Me.rbAllUsers.TabIndex = 10
        Me.rbAllUsers.TabStop = True
        Me.rbAllUsers.Text = "Alle"
        '
        'dtToDate
        '
        Me.dtToDate.CustomFormat = "yyyyMMdd"
        Me.dtToDate.Location = New System.Drawing.Point(256, 8)
        Me.dtToDate.Name = "dtToDate"
        Me.dtToDate.Size = New System.Drawing.Size(128, 20)
        Me.dtToDate.TabIndex = 8
        '
        'dtFromDate
        '
        Me.dtFromDate.CustomFormat = "yyyyMMdd"
        Me.dtFromDate.Location = New System.Drawing.Point(64, 8)
        Me.dtFromDate.Name = "dtFromDate"
        Me.dtFromDate.Size = New System.Drawing.Size(128, 20)
        Me.dtFromDate.TabIndex = 7
        '
        'Label39
        '
        Me.Label39.Location = New System.Drawing.Point(200, 8)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(56, 23)
        Me.Label39.TabIndex = 5
        Me.Label39.Text = "Til dato:"
        '
        'Label38
        '
        Me.Label38.Location = New System.Drawing.Point(8, 8)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(56, 23)
        Me.Label38.TabIndex = 4
        Me.Label38.Text = "Fra dato:"
        '
        'cbUsers
        '
        Me.cbUsers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbUsers.Items.AddRange(New Object() {"Alle"})
        Me.cbUsers.Location = New System.Drawing.Point(520, 8)
        Me.cbUsers.Name = "cbUsers"
        Me.cbUsers.Size = New System.Drawing.Size(200, 21)
        Me.cbUsers.TabIndex = 3
        '
        'tpStats
        '
        Me.tpStats.Controls.Add(Me.dgStats)
        Me.tpStats.Controls.Add(Me.Panel8)
        Me.tpStats.Location = New System.Drawing.Point(4, 22)
        Me.tpStats.Name = "tpStats"
        Me.tpStats.Size = New System.Drawing.Size(832, 556)
        Me.tpStats.TabIndex = 1
        Me.tpStats.Text = "Sidevisninger"
        '
        'dgStats
        '
        Me.dgStats.AlternatingBackColor = System.Drawing.Color.WhiteSmoke
        Me.dgStats.BackColor = System.Drawing.Color.Gainsboro
        Me.dgStats.BackgroundColor = System.Drawing.Color.DarkGray
        Me.dgStats.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.dgStats.CaptionBackColor = System.Drawing.Color.DarkKhaki
        Me.dgStats.CaptionFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.dgStats.CaptionForeColor = System.Drawing.Color.Black
        Me.dgStats.DataMember = ""
        Me.dgStats.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgStats.FlatMode = True
        Me.dgStats.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.dgStats.ForeColor = System.Drawing.Color.Black
        Me.dgStats.GridLineColor = System.Drawing.Color.Silver
        Me.dgStats.HeaderBackColor = System.Drawing.Color.Black
        Me.dgStats.HeaderFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.dgStats.HeaderForeColor = System.Drawing.Color.White
        Me.dgStats.LinkColor = System.Drawing.Color.DarkSlateBlue
        Me.dgStats.Location = New System.Drawing.Point(0, 40)
        Me.dgStats.Name = "dgStats"
        Me.dgStats.ParentRowsBackColor = System.Drawing.Color.LightGray
        Me.dgStats.ParentRowsForeColor = System.Drawing.Color.Black
        Me.dgStats.PreferredColumnWidth = 150
        Me.dgStats.SelectionBackColor = System.Drawing.Color.Firebrick
        Me.dgStats.SelectionForeColor = System.Drawing.Color.White
        Me.dgStats.Size = New System.Drawing.Size(832, 516)
        Me.dgStats.TabIndex = 1
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.Panel12)
        Me.Panel8.Controls.Add(Me.dtToDate2)
        Me.Panel8.Controls.Add(Me.dtFromDate2)
        Me.Panel8.Controls.Add(Me.Label40)
        Me.Panel8.Controls.Add(Me.Label41)
        Me.Panel8.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel8.Location = New System.Drawing.Point(0, 0)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(832, 40)
        Me.Panel8.TabIndex = 0
        '
        'Panel12
        '
        Me.Panel12.Controls.Add(Me.btnShowPageStats)
        Me.Panel12.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel12.Location = New System.Drawing.Point(744, 0)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(88, 40)
        Me.Panel12.TabIndex = 10
        '
        'btnShowPageStats
        '
        Me.btnShowPageStats.Location = New System.Drawing.Point(8, 8)
        Me.btnShowPageStats.Name = "btnShowPageStats"
        Me.btnShowPageStats.TabIndex = 3
        Me.btnShowPageStats.Text = "Vis"
        '
        'dtToDate2
        '
        Me.dtToDate2.CustomFormat = "yyyyMMdd"
        Me.dtToDate2.Location = New System.Drawing.Point(256, 8)
        Me.dtToDate2.Name = "dtToDate2"
        Me.dtToDate2.Size = New System.Drawing.Size(128, 20)
        Me.dtToDate2.TabIndex = 9
        '
        'dtFromDate2
        '
        Me.dtFromDate2.CustomFormat = "yyyyMMdd"
        Me.dtFromDate2.Location = New System.Drawing.Point(64, 8)
        Me.dtFromDate2.Name = "dtFromDate2"
        Me.dtFromDate2.Size = New System.Drawing.Size(128, 20)
        Me.dtFromDate2.TabIndex = 8
        '
        'Label40
        '
        Me.Label40.Location = New System.Drawing.Point(200, 8)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(56, 23)
        Me.Label40.TabIndex = 7
        Me.Label40.Text = "Til dato:"
        '
        'Label41
        '
        Me.Label41.Location = New System.Drawing.Point(8, 8)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(56, 23)
        Me.Label41.TabIndex = 6
        Me.Label41.Text = "Fra dato:"
        '
        'tpUserStats
        '
        Me.tpUserStats.Controls.Add(Me.dgUserStats)
        Me.tpUserStats.Controls.Add(Me.Panel13)
        Me.tpUserStats.Location = New System.Drawing.Point(4, 22)
        Me.tpUserStats.Name = "tpUserStats"
        Me.tpUserStats.Size = New System.Drawing.Size(832, 556)
        Me.tpUserStats.TabIndex = 2
        Me.tpUserStats.Text = "Brukerstatistikk"
        '
        'dgUserStats
        '
        Me.dgUserStats.AlternatingBackColor = System.Drawing.Color.WhiteSmoke
        Me.dgUserStats.BackColor = System.Drawing.Color.Gainsboro
        Me.dgUserStats.BackgroundColor = System.Drawing.Color.DarkGray
        Me.dgUserStats.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.dgUserStats.CaptionBackColor = System.Drawing.Color.DarkKhaki
        Me.dgUserStats.CaptionFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.dgUserStats.CaptionForeColor = System.Drawing.Color.Black
        Me.dgUserStats.DataMember = ""
        Me.dgUserStats.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgUserStats.FlatMode = True
        Me.dgUserStats.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.dgUserStats.ForeColor = System.Drawing.Color.Black
        Me.dgUserStats.GridLineColor = System.Drawing.Color.Silver
        Me.dgUserStats.HeaderBackColor = System.Drawing.Color.Black
        Me.dgUserStats.HeaderFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.dgUserStats.HeaderForeColor = System.Drawing.Color.White
        Me.dgUserStats.LinkColor = System.Drawing.Color.DarkSlateBlue
        Me.dgUserStats.Location = New System.Drawing.Point(0, 40)
        Me.dgUserStats.Name = "dgUserStats"
        Me.dgUserStats.ParentRowsBackColor = System.Drawing.Color.LightGray
        Me.dgUserStats.ParentRowsForeColor = System.Drawing.Color.Black
        Me.dgUserStats.PreferredColumnWidth = 125
        Me.dgUserStats.SelectionBackColor = System.Drawing.Color.Firebrick
        Me.dgUserStats.SelectionForeColor = System.Drawing.Color.White
        Me.dgUserStats.Size = New System.Drawing.Size(832, 516)
        Me.dgUserStats.TabIndex = 2
        '
        'Panel13
        '
        Me.Panel13.Controls.Add(Me.Panel14)
        Me.Panel13.Controls.Add(Me.rbUsUser)
        Me.Panel13.Controls.Add(Me.rbUsAll)
        Me.Panel13.Controls.Add(Me.dtToDate3)
        Me.Panel13.Controls.Add(Me.dtFromDate3)
        Me.Panel13.Controls.Add(Me.Label61)
        Me.Panel13.Controls.Add(Me.Label62)
        Me.Panel13.Controls.Add(Me.cbUsUser)
        Me.Panel13.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel13.Location = New System.Drawing.Point(0, 0)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(832, 40)
        Me.Panel13.TabIndex = 1
        '
        'Panel14
        '
        Me.Panel14.Controls.Add(Me.btnShowUserStats)
        Me.Panel14.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel14.Location = New System.Drawing.Point(744, 0)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(88, 40)
        Me.Panel14.TabIndex = 12
        '
        'btnShowUserStats
        '
        Me.btnShowUserStats.Location = New System.Drawing.Point(8, 8)
        Me.btnShowUserStats.Name = "btnShowUserStats"
        Me.btnShowUserStats.TabIndex = 0
        Me.btnShowUserStats.Text = "Vis"
        '
        'rbUsUser
        '
        Me.rbUsUser.Location = New System.Drawing.Point(448, 8)
        Me.rbUsUser.Name = "rbUsUser"
        Me.rbUsUser.Size = New System.Drawing.Size(72, 24)
        Me.rbUsUser.TabIndex = 11
        Me.rbUsUser.Text = "Bruker:"
        '
        'rbUsAll
        '
        Me.rbUsAll.Checked = True
        Me.rbUsAll.Location = New System.Drawing.Point(392, 8)
        Me.rbUsAll.Name = "rbUsAll"
        Me.rbUsAll.Size = New System.Drawing.Size(56, 24)
        Me.rbUsAll.TabIndex = 10
        Me.rbUsAll.TabStop = True
        Me.rbUsAll.Text = "Alle"
        '
        'dtToDate3
        '
        Me.dtToDate3.CustomFormat = "yyyyMMdd"
        Me.dtToDate3.Location = New System.Drawing.Point(256, 8)
        Me.dtToDate3.Name = "dtToDate3"
        Me.dtToDate3.Size = New System.Drawing.Size(128, 20)
        Me.dtToDate3.TabIndex = 8
        '
        'dtFromDate3
        '
        Me.dtFromDate3.CustomFormat = "yyyyMMdd"
        Me.dtFromDate3.Location = New System.Drawing.Point(64, 8)
        Me.dtFromDate3.Name = "dtFromDate3"
        Me.dtFromDate3.Size = New System.Drawing.Size(128, 20)
        Me.dtFromDate3.TabIndex = 7
        '
        'Label61
        '
        Me.Label61.Location = New System.Drawing.Point(200, 8)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(56, 23)
        Me.Label61.TabIndex = 5
        Me.Label61.Text = "Til dato:"
        '
        'Label62
        '
        Me.Label62.Location = New System.Drawing.Point(8, 8)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(56, 23)
        Me.Label62.TabIndex = 4
        Me.Label62.Text = "Fra dato:"
        '
        'cbUsUser
        '
        Me.cbUsUser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbUsUser.Items.AddRange(New Object() {"Alle"})
        Me.cbUsUser.Location = New System.Drawing.Point(520, 8)
        Me.cbUsUser.Name = "cbUsUser"
        Me.cbUsUser.Size = New System.Drawing.Size(200, 21)
        Me.cbUsUser.TabIndex = 3
        '
        'tbUser
        '
        Me.tbUser.Controls.Add(Me.Panel9)
        Me.tbUser.Controls.Add(Me.Panel1)
        Me.tbUser.Controls.Add(Me.ListBox1)
        Me.tbUser.Location = New System.Drawing.Point(4, 22)
        Me.tbUser.Name = "tbUser"
        Me.tbUser.Size = New System.Drawing.Size(840, 582)
        Me.tbUser.TabIndex = 4
        Me.tbUser.Text = "Brukere"
        '
        'Panel9
        '
        Me.Panel9.Controls.Add(Me.btnRefreshUsers)
        Me.Panel9.Controls.Add(Me.btnDeleteUser)
        Me.Panel9.Controls.Add(Me.btnNewUser)
        Me.Panel9.Controls.Add(Me.btnEditUser)
        Me.Panel9.Controls.Add(Me.pUser)
        Me.Panel9.Controls.Add(Me.lbUsers)
        Me.Panel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel9.Location = New System.Drawing.Point(0, 40)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(840, 542)
        Me.Panel9.TabIndex = 6
        '
        'btnRefreshUsers
        '
        Me.btnRefreshUsers.Location = New System.Drawing.Point(8, 504)
        Me.btnRefreshUsers.Name = "btnRefreshUsers"
        Me.btnRefreshUsers.TabIndex = 10
        Me.btnRefreshUsers.Text = "Oppdater"
        '
        'btnDeleteUser
        '
        Me.btnDeleteUser.Location = New System.Drawing.Point(168, 472)
        Me.btnDeleteUser.Name = "btnDeleteUser"
        Me.btnDeleteUser.TabIndex = 9
        Me.btnDeleteUser.Text = "Slett"
        '
        'btnNewUser
        '
        Me.btnNewUser.Location = New System.Drawing.Point(88, 472)
        Me.btnNewUser.Name = "btnNewUser"
        Me.btnNewUser.TabIndex = 8
        Me.btnNewUser.Text = "Ny"
        '
        'btnEditUser
        '
        Me.btnEditUser.Location = New System.Drawing.Point(8, 472)
        Me.btnEditUser.Name = "btnEditUser"
        Me.btnEditUser.TabIndex = 6
        Me.btnEditUser.Text = "Rediger"
        '
        'pUser
        '
        Me.pUser.Controls.Add(Me.interval)
        Me.pUser.Controls.Add(Me.Label63)
        Me.pUser.Controls.Add(Me.Expire)
        Me.pUser.Controls.Add(Me.ExpireDate)
        Me.pUser.Controls.Add(Me.chkIPPwd)
        Me.pUser.Controls.Add(Me.Label60)
        Me.pUser.Controls.Add(Me.lblCreated)
        Me.pUser.Controls.Add(Me.Label59)
        Me.pUser.Controls.Add(Me.createdBy)
        Me.pUser.Controls.Add(Me.adresse)
        Me.pUser.Controls.Add(Me.Label58)
        Me.pUser.Controls.Add(Me.Label57)
        Me.pUser.Controls.Add(Me.telefon)
        Me.pUser.Controls.Add(Me.Label56)
        Me.pUser.Controls.Add(Me.Label53)
        Me.pUser.Controls.Add(Me.Label55)
        Me.pUser.Controls.Add(Me.lblLastloggedin)
        Me.pUser.Controls.Add(Me.txtNote)
        Me.pUser.Controls.Add(Me.Label54)
        Me.pUser.Controls.Add(Me.chkActive)
        Me.pUser.Controls.Add(Me.btnCancelUser)
        Me.pUser.Controls.Add(Me.txtIPRange)
        Me.pUser.Controls.Add(Me.Label44)
        Me.pUser.Controls.Add(Me.txtName)
        Me.pUser.Controls.Add(Me.Label43)
        Me.pUser.Controls.Add(Me.Label48)
        Me.pUser.Controls.Add(Me.txtEmail)
        Me.pUser.Controls.Add(Me.Label42)
        Me.pUser.Controls.Add(Me.txtPassword)
        Me.pUser.Controls.Add(Me.Label46)
        Me.pUser.Controls.Add(Me.Label47)
        Me.pUser.Controls.Add(Me.Label49)
        Me.pUser.Controls.Add(Me.Label50)
        Me.pUser.Controls.Add(Me.Label51)
        Me.pUser.Controls.Add(Me.lblId)
        Me.pUser.Controls.Add(Me.txtUserId)
        Me.pUser.Controls.Add(Me.chkAdmin)
        Me.pUser.Controls.Add(Me.chkInvoice)
        Me.pUser.Controls.Add(Me.lbUserMainCat)
        Me.pUser.Controls.Add(Me.lbUserMaingroup)
        Me.pUser.Controls.Add(Me.btnSaveUser)
        Me.pUser.Location = New System.Drawing.Point(256, 8)
        Me.pUser.Name = "pUser"
        Me.pUser.Size = New System.Drawing.Size(568, 544)
        Me.pUser.TabIndex = 5
        Me.pUser.Visible = False
        '
        'interval
        '
        Me.interval.Location = New System.Drawing.Point(440, 136)
        Me.interval.Name = "interval"
        Me.interval.Size = New System.Drawing.Size(120, 20)
        Me.interval.TabIndex = 78
        Me.interval.Text = "0"
        '
        'Label63
        '
        Me.Label63.Location = New System.Drawing.Point(288, 136)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(136, 20)
        Me.Label63.TabIndex = 77
        Me.Label63.Text = "Passordendringsinterval:"
        Me.Label63.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Expire
        '
        Me.Expire.Location = New System.Drawing.Point(288, 104)
        Me.Expire.Name = "Expire"
        Me.Expire.TabIndex = 76
        Me.Expire.Text = "Bruker gyldig til:"
        '
        'ExpireDate
        '
        Me.ExpireDate.Enabled = False
        Me.ExpireDate.Location = New System.Drawing.Point(408, 104)
        Me.ExpireDate.Name = "ExpireDate"
        Me.ExpireDate.Size = New System.Drawing.Size(152, 20)
        Me.ExpireDate.TabIndex = 75
        '
        'chkIPPwd
        '
        Me.chkIPPwd.Location = New System.Drawing.Point(288, 80)
        Me.chkIPPwd.Name = "chkIPPwd"
        Me.chkIPPwd.Size = New System.Drawing.Size(192, 24)
        Me.chkIPPwd.TabIndex = 74
        Me.chkIPPwd.Text = "Sjekk b�de IP-range og passord"
        '
        'Label60
        '
        Me.Label60.Location = New System.Drawing.Point(288, 168)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(64, 23)
        Me.Label60.TabIndex = 73
        Me.Label60.Text = "Opprettet:"
        '
        'lblCreated
        '
        Me.lblCreated.Location = New System.Drawing.Point(368, 168)
        Me.lblCreated.Name = "lblCreated"
        Me.lblCreated.Size = New System.Drawing.Size(192, 23)
        Me.lblCreated.TabIndex = 72
        '
        'Label59
        '
        Me.Label59.Location = New System.Drawing.Point(288, 192)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(112, 23)
        Me.Label59.TabIndex = 71
        Me.Label59.Text = "Opprettet/endret av:"
        '
        'createdBy
        '
        Me.createdBy.Location = New System.Drawing.Point(400, 192)
        Me.createdBy.Name = "createdBy"
        Me.createdBy.Size = New System.Drawing.Size(160, 20)
        Me.createdBy.TabIndex = 70
        Me.createdBy.Text = ""
        '
        'adresse
        '
        Me.adresse.Location = New System.Drawing.Point(288, 232)
        Me.adresse.Multiline = True
        Me.adresse.Name = "adresse"
        Me.adresse.Size = New System.Drawing.Size(272, 72)
        Me.adresse.TabIndex = 68
        Me.adresse.Text = ""
        '
        'Label58
        '
        Me.Label58.Location = New System.Drawing.Point(288, 216)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(136, 23)
        Me.Label58.TabIndex = 69
        Me.Label58.Text = "Faktura-adresse:"
        '
        'Label57
        '
        Me.Label57.Location = New System.Drawing.Point(8, 152)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(88, 23)
        Me.Label57.TabIndex = 67
        Me.Label57.Text = "Telefon:"
        '
        'telefon
        '
        Me.telefon.Location = New System.Drawing.Point(96, 152)
        Me.telefon.Name = "telefon"
        Me.telefon.Size = New System.Drawing.Size(160, 20)
        Me.telefon.TabIndex = 66
        Me.telefon.Text = ""
        '
        'Label56
        '
        Me.Label56.Location = New System.Drawing.Point(8, 384)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(80, 40)
        Me.Label56.TabIndex = 65
        Me.Label56.Text = "(ingen valgte gir alle kategorier)"
        '
        'Label53
        '
        Me.Label53.Location = New System.Drawing.Point(8, 256)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(80, 40)
        Me.Label53.TabIndex = 64
        Me.Label53.Text = "(ingen valgte gir alle stoffgrupper)"
        Me.Label53.Visible = False
        '
        'Label55
        '
        Me.Label55.Location = New System.Drawing.Point(8, 32)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(80, 23)
        Me.Label55.TabIndex = 63
        Me.Label55.Text = "Sist innlogget:"
        '
        'lblLastloggedin
        '
        Me.lblLastloggedin.Location = New System.Drawing.Point(96, 32)
        Me.lblLastloggedin.Name = "lblLastloggedin"
        Me.lblLastloggedin.Size = New System.Drawing.Size(160, 23)
        Me.lblLastloggedin.TabIndex = 62
        '
        'txtNote
        '
        Me.txtNote.Location = New System.Drawing.Point(288, 400)
        Me.txtNote.Multiline = True
        Me.txtNote.Name = "txtNote"
        Me.txtNote.Size = New System.Drawing.Size(272, 96)
        Me.txtNote.TabIndex = 60
        Me.txtNote.Text = ""
        '
        'Label54
        '
        Me.Label54.Location = New System.Drawing.Point(288, 384)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(88, 23)
        Me.Label54.TabIndex = 61
        Me.Label54.Text = "Notat:"
        '
        'chkActive
        '
        Me.chkActive.Location = New System.Drawing.Point(288, 8)
        Me.chkActive.Name = "chkActive"
        Me.chkActive.Size = New System.Drawing.Size(120, 24)
        Me.chkActive.TabIndex = 59
        Me.chkActive.Text = "Aktiv"
        '
        'btnCancelUser
        '
        Me.btnCancelUser.Location = New System.Drawing.Point(88, 496)
        Me.btnCancelUser.Name = "btnCancelUser"
        Me.btnCancelUser.TabIndex = 58
        Me.btnCancelUser.Text = "Avbryt"
        '
        'txtIPRange
        '
        Me.txtIPRange.Location = New System.Drawing.Point(288, 328)
        Me.txtIPRange.Multiline = True
        Me.txtIPRange.Name = "txtIPRange"
        Me.txtIPRange.Size = New System.Drawing.Size(272, 40)
        Me.txtIPRange.TabIndex = 54
        Me.txtIPRange.Text = ""
        '
        'Label44
        '
        Me.Label44.Location = New System.Drawing.Point(8, 120)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(88, 23)
        Me.Label44.TabIndex = 57
        Me.Label44.Text = "Navn:"
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(96, 120)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(160, 20)
        Me.txtName.TabIndex = 56
        Me.txtName.Text = ""
        '
        'Label43
        '
        Me.Label43.Location = New System.Drawing.Point(288, 312)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(88, 23)
        Me.Label43.TabIndex = 55
        Me.Label43.Text = "IP-range:"
        '
        'Label48
        '
        Me.Label48.Location = New System.Drawing.Point(8, 184)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(88, 23)
        Me.Label48.TabIndex = 53
        Me.Label48.Text = "Epost:"
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(96, 184)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(160, 20)
        Me.txtEmail.TabIndex = 52
        Me.txtEmail.Text = ""
        '
        'Label42
        '
        Me.Label42.Location = New System.Drawing.Point(8, 88)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(88, 23)
        Me.Label42.TabIndex = 51
        Me.Label42.Text = "Passord:"
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(96, 88)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(160, 20)
        Me.txtPassword.TabIndex = 50
        Me.txtPassword.Text = ""
        '
        'Label46
        '
        Me.Label46.Location = New System.Drawing.Point(8, 360)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(88, 23)
        Me.Label46.TabIndex = 46
        Me.Label46.Text = "Kategori:"
        '
        'Label47
        '
        Me.Label47.Location = New System.Drawing.Point(8, 216)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(88, 32)
        Me.Label47.TabIndex = 45
        Me.Label47.Text = "Produkter / Stoffgrupper:"
        '
        'Label49
        '
        Me.Label49.Location = New System.Drawing.Point(8, 56)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(88, 23)
        Me.Label49.TabIndex = 43
        Me.Label49.Text = "BrukerId:"
        '
        'Label50
        '
        Me.Label50.Location = New System.Drawing.Point(8, 8)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(88, 23)
        Me.Label50.TabIndex = 42
        Me.Label50.Text = "Id:"
        '
        'Label51
        '
        Me.Label51.Location = New System.Drawing.Point(264, 56)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(24, 23)
        Me.Label51.TabIndex = 41
        '
        'lblId
        '
        Me.lblId.Location = New System.Drawing.Point(96, 8)
        Me.lblId.Name = "lblId"
        Me.lblId.Size = New System.Drawing.Size(128, 23)
        Me.lblId.TabIndex = 40
        '
        'txtUserId
        '
        Me.txtUserId.Location = New System.Drawing.Point(96, 56)
        Me.txtUserId.Name = "txtUserId"
        Me.txtUserId.Size = New System.Drawing.Size(160, 20)
        Me.txtUserId.TabIndex = 39
        Me.txtUserId.Text = ""
        '
        'chkAdmin
        '
        Me.chkAdmin.Location = New System.Drawing.Point(288, 32)
        Me.chkAdmin.Name = "chkAdmin"
        Me.chkAdmin.TabIndex = 33
        Me.chkAdmin.Text = "Administrator"
        '
        'chkInvoice
        '
        Me.chkInvoice.Location = New System.Drawing.Point(288, 56)
        Me.chkInvoice.Name = "chkInvoice"
        Me.chkInvoice.Size = New System.Drawing.Size(120, 24)
        Me.chkInvoice.TabIndex = 32
        Me.chkInvoice.Text = "Stykk faktureres"
        '
        'lbUserMainCat
        '
        Me.lbUserMainCat.Location = New System.Drawing.Point(96, 360)
        Me.lbUserMainCat.Name = "lbUserMainCat"
        Me.lbUserMainCat.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.lbUserMainCat.Size = New System.Drawing.Size(160, 134)
        Me.lbUserMainCat.TabIndex = 31
        '
        'lbUserMaingroup
        '
        Me.lbUserMaingroup.Location = New System.Drawing.Point(96, 216)
        Me.lbUserMaingroup.Name = "lbUserMaingroup"
        Me.lbUserMaingroup.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.lbUserMaingroup.Size = New System.Drawing.Size(160, 134)
        Me.lbUserMaingroup.TabIndex = 30
        '
        'btnSaveUser
        '
        Me.btnSaveUser.Location = New System.Drawing.Point(8, 496)
        Me.btnSaveUser.Name = "btnSaveUser"
        Me.btnSaveUser.TabIndex = 7
        Me.btnSaveUser.Text = "Lagre"
        '
        'lbUsers
        '
        Me.lbUsers.Location = New System.Drawing.Point(8, 0)
        Me.lbUsers.Name = "lbUsers"
        Me.lbUsers.Size = New System.Drawing.Size(240, 459)
        Me.lbUsers.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.onlySG)
        Me.Panel1.Controls.Add(Me.allUsers)
        Me.Panel1.Controls.Add(Me.cbShowUserSG)
        Me.Panel1.Controls.Add(Me.txtSearchUser)
        Me.Panel1.Controls.Add(Me.Label15)
        Me.Panel1.Controls.Add(Me.btnFindUser)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(840, 40)
        Me.Panel1.TabIndex = 3
        '
        'onlySG
        '
        Me.onlySG.Location = New System.Drawing.Point(80, 8)
        Me.onlySG.Name = "onlySG"
        Me.onlySG.Size = New System.Drawing.Size(184, 24)
        Me.onlySG.TabIndex = 21
        Me.onlySG.Text = "Vis bare brukere med tilgang til:"
        '
        'allUsers
        '
        Me.allUsers.Checked = True
        Me.allUsers.Location = New System.Drawing.Point(8, 8)
        Me.allUsers.Name = "allUsers"
        Me.allUsers.Size = New System.Drawing.Size(64, 24)
        Me.allUsers.TabIndex = 20
        Me.allUsers.TabStop = True
        Me.allUsers.Text = "Vis alle"
        '
        'cbShowUserSG
        '
        Me.cbShowUserSG.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbShowUserSG.Items.AddRange(New Object() {"Alle stoffgrupper"})
        Me.cbShowUserSG.Location = New System.Drawing.Point(264, 8)
        Me.cbShowUserSG.Name = "cbShowUserSG"
        Me.cbShowUserSG.Size = New System.Drawing.Size(200, 21)
        Me.cbShowUserSG.TabIndex = 19
        '
        'txtSearchUser
        '
        Me.txtSearchUser.Location = New System.Drawing.Point(544, 8)
        Me.txtSearchUser.Name = "txtSearchUser"
        Me.txtSearchUser.Size = New System.Drawing.Size(128, 20)
        Me.txtSearchUser.TabIndex = 15
        Me.txtSearchUser.Text = ""
        '
        'Label15
        '
        Me.Label15.Location = New System.Drawing.Point(488, 8)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(56, 23)
        Me.Label15.TabIndex = 17
        Me.Label15.Text = "S�k etter:"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnFindUser
        '
        Me.btnFindUser.Location = New System.Drawing.Point(680, 8)
        Me.btnFindUser.Name = "btnFindUser"
        Me.btnFindUser.Size = New System.Drawing.Size(144, 23)
        Me.btnFindUser.TabIndex = 16
        Me.btnFindUser.Text = "Vis utvalg / S�k"
        '
        'ListBox1
        '
        Me.ListBox1.Location = New System.Drawing.Point(8, 120)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(232, 381)
        Me.ListBox1.TabIndex = 19
        '
        'tbArticle
        '
        Me.tbArticle.Controls.Add(Me.dgArticles)
        Me.tbArticle.Controls.Add(Me.Panel3)
        Me.tbArticle.Location = New System.Drawing.Point(4, 22)
        Me.tbArticle.Name = "tbArticle"
        Me.tbArticle.Size = New System.Drawing.Size(840, 582)
        Me.tbArticle.TabIndex = 2
        Me.tbArticle.Text = "Artikler"
        '
        'dgArticles
        '
        Me.dgArticles.AlternatingBackColor = System.Drawing.Color.WhiteSmoke
        Me.dgArticles.BackColor = System.Drawing.Color.Gainsboro
        Me.dgArticles.BackgroundColor = System.Drawing.Color.DarkGray
        Me.dgArticles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.dgArticles.CaptionBackColor = System.Drawing.Color.DarkKhaki
        Me.dgArticles.CaptionFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.dgArticles.CaptionForeColor = System.Drawing.Color.Black
        Me.dgArticles.DataMember = ""
        Me.dgArticles.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgArticles.FlatMode = True
        Me.dgArticles.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.dgArticles.ForeColor = System.Drawing.Color.Black
        Me.dgArticles.GridLineColor = System.Drawing.Color.Silver
        Me.dgArticles.HeaderBackColor = System.Drawing.Color.Black
        Me.dgArticles.HeaderFont = New System.Drawing.Font("Microsoft Sans Serif", 8.0!)
        Me.dgArticles.HeaderForeColor = System.Drawing.Color.White
        Me.dgArticles.LinkColor = System.Drawing.Color.DarkSlateBlue
        Me.dgArticles.Location = New System.Drawing.Point(0, 40)
        Me.dgArticles.Name = "dgArticles"
        Me.dgArticles.ParentRowsBackColor = System.Drawing.Color.LightGray
        Me.dgArticles.ParentRowsForeColor = System.Drawing.Color.Black
        Me.dgArticles.PreferredColumnWidth = 125
        Me.dgArticles.SelectionBackColor = System.Drawing.Color.Firebrick
        Me.dgArticles.SelectionForeColor = System.Drawing.Color.White
        Me.dgArticles.Size = New System.Drawing.Size(840, 542)
        Me.dgArticles.TabIndex = 2
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.Panel10)
        Me.Panel3.Controls.Add(Me.cbInterval)
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.DateTimePicker1)
        Me.Panel3.Controls.Add(Me.Label6)
        Me.Panel3.Controls.Add(Me.cbCategories)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel3.Location = New System.Drawing.Point(0, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(840, 40)
        Me.Panel3.TabIndex = 1
        '
        'Panel10
        '
        Me.Panel10.Controls.Add(Me.btnDeleteArticle)
        Me.Panel10.Controls.Add(Me.btnShowArticles)
        Me.Panel10.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel10.Location = New System.Drawing.Point(672, 0)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(168, 40)
        Me.Panel10.TabIndex = 15
        '
        'btnDeleteArticle
        '
        Me.btnDeleteArticle.Location = New System.Drawing.Point(88, 8)
        Me.btnDeleteArticle.Name = "btnDeleteArticle"
        Me.btnDeleteArticle.TabIndex = 13
        Me.btnDeleteArticle.Text = "Slett"
        '
        'btnShowArticles
        '
        Me.btnShowArticles.Location = New System.Drawing.Point(8, 8)
        Me.btnShowArticles.Name = "btnShowArticles"
        Me.btnShowArticles.TabIndex = 0
        Me.btnShowArticles.Text = "Vis"
        '
        'cbInterval
        '
        Me.cbInterval.Items.AddRange(New Object() {"12", "24", "48", "72"})
        Me.cbInterval.Location = New System.Drawing.Point(104, 8)
        Me.cbInterval.Name = "cbInterval"
        Me.cbInterval.Size = New System.Drawing.Size(80, 21)
        Me.cbInterval.TabIndex = 14
        Me.cbInterval.Text = "24"
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(8, 8)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(88, 23)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "Intervall (timer):"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Location = New System.Drawing.Point(432, 8)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.TabIndex = 11
        Me.DateTimePicker1.Visible = False
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(240, 8)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(64, 23)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Kategori:"
        Me.Label6.Visible = False
        '
        'cbCategories
        '
        Me.cbCategories.Location = New System.Drawing.Point(304, 8)
        Me.cbCategories.Name = "cbCategories"
        Me.cbCategories.Size = New System.Drawing.Size(121, 21)
        Me.cbCategories.TabIndex = 1
        Me.cbCategories.Text = "ComboBox2"
        Me.cbCategories.Visible = False
        '
        'tbMenu
        '
        Me.tbMenu.Controls.Add(Me.lbMenues)
        Me.tbMenu.Controls.Add(Me.TextBox3)
        Me.tbMenu.Controls.Add(Me.Label5)
        Me.tbMenu.Controls.Add(Me.Button11)
        Me.tbMenu.Controls.Add(Me.Button14)
        Me.tbMenu.Controls.Add(Me.Button15)
        Me.tbMenu.Controls.Add(Me.Button16)
        Me.tbMenu.Location = New System.Drawing.Point(4, 22)
        Me.tbMenu.Name = "tbMenu"
        Me.tbMenu.Size = New System.Drawing.Size(840, 582)
        Me.tbMenu.TabIndex = 1
        Me.tbMenu.Text = "Menyer"
        '
        'lbMenues
        '
        Me.lbMenues.Location = New System.Drawing.Point(8, 40)
        Me.lbMenues.Name = "lbMenues"
        Me.lbMenues.Size = New System.Drawing.Size(240, 381)
        Me.lbMenues.TabIndex = 18
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(64, 8)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(88, 20)
        Me.TextBox3.TabIndex = 16
        Me.TextBox3.Text = ""
        '
        'Label5
        '
        Me.Label5.Location = New System.Drawing.Point(8, 8)
        Me.Label5.Name = "Label5"
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "S�k:"
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(168, 8)
        Me.Button11.Name = "Button11"
        Me.Button11.TabIndex = 15
        Me.Button11.Text = "S�k"
        '
        'Button14
        '
        Me.Button14.Location = New System.Drawing.Point(168, 432)
        Me.Button14.Name = "Button14"
        Me.Button14.TabIndex = 13
        Me.Button14.Text = "Slett"
        '
        'Button15
        '
        Me.Button15.Location = New System.Drawing.Point(88, 432)
        Me.Button15.Name = "Button15"
        Me.Button15.TabIndex = 12
        Me.Button15.Text = "Ny"
        '
        'Button16
        '
        Me.Button16.Location = New System.Drawing.Point(8, 432)
        Me.Button16.Name = "Button16"
        Me.Button16.TabIndex = 11
        Me.Button16.Text = "Rediger"
        '
        'Label13
        '
        Me.Label13.Location = New System.Drawing.Point(240, 96)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(80, 16)
        Me.Label13.TabIndex = 28
        '
        'Label16
        '
        Me.Label16.Location = New System.Drawing.Point(8, 368)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(88, 23)
        Me.Label16.TabIndex = 19
        '
        'Label14
        '
        Me.Label14.Location = New System.Drawing.Point(8, 336)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(88, 23)
        Me.Label14.TabIndex = 18
        '
        'Label12
        '
        Me.Label12.Location = New System.Drawing.Point(8, 304)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(88, 23)
        Me.Label12.TabIndex = 17
        '
        'Label11
        '
        Me.Label11.Location = New System.Drawing.Point(8, 200)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(88, 23)
        Me.Label11.TabIndex = 16
        '
        'Label10
        '
        Me.Label10.Location = New System.Drawing.Point(8, 96)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(88, 23)
        Me.Label10.TabIndex = 15
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(8, 64)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(88, 23)
        Me.Label9.TabIndex = 14
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(8, 32)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(88, 23)
        Me.Label8.TabIndex = 13
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(8, 8)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(88, 23)
        Me.Label4.TabIndex = 12
        '
        'Label19
        '
        Me.Label19.Location = New System.Drawing.Point(8, 160)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(56, 23)
        Me.Label19.TabIndex = 4
        '
        'Label18
        '
        Me.Label18.Location = New System.Drawing.Point(8, 40)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(48, 23)
        Me.Label18.TabIndex = 2
        '
        'Label17
        '
        Me.Label17.Location = New System.Drawing.Point(8, 8)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(48, 23)
        Me.Label17.TabIndex = 0
        '
        'web
        '
        Me.web.ContainingControl = Me
        Me.web.Enabled = True
        Me.web.Location = New System.Drawing.Point(16, 32)
        Me.web.OcxState = CType(resources.GetObject("web.OcxState"), System.Windows.Forms.AxHost.State)
        Me.web.Size = New System.Drawing.Size(544, 352)
        Me.web.TabIndex = 28
        Me.web.Visible = False
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 16)
        Me.Label1.TabIndex = 27
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(-4, -65)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 33)
        Me.Label2.TabIndex = 6
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.Filter = "All files (*.*)|*.*"
        Me.OpenFileDialog1.Title = "Last opp bilde"
        '
        'daSection
        '
        Me.daSection.DeleteCommand = Me.SqlDeleteCommand1
        Me.daSection.InsertCommand = Me.SqlInsertCommand1
        Me.daSection.SelectCommand = Me.SqlSelectCommand1
        Me.daSection.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "SECTIONS", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("SiteCode", "SiteCode"), New System.Data.Common.DataColumnMapping("MainGroup", "MainGroup"), New System.Data.Common.DataColumnMapping("SubGroup", "SubGroup"), New System.Data.Common.DataColumnMapping("Name", "Name"), New System.Data.Common.DataColumnMapping("Lastmodified", "Lastmodified"), New System.Data.Common.DataColumnMapping("Content", "Content"), New System.Data.Common.DataColumnMapping("TitleGif", "TitleGif"), New System.Data.Common.DataColumnMapping("Picture", "Picture"), New System.Data.Common.DataColumnMapping("PictureHTML", "PictureHTML"), New System.Data.Common.DataColumnMapping("PictureWidth", "PictureWidth"), New System.Data.Common.DataColumnMapping("PictureHeight", "PictureHeight"), New System.Data.Common.DataColumnMapping("PictureHeading", "PictureHeading"), New System.Data.Common.DataColumnMapping("PictureText", "PictureText"), New System.Data.Common.DataColumnMapping("NumArticles", "NumArticles"), New System.Data.Common.DataColumnMapping("PageCount", "PageCount"), New System.Data.Common.DataColumnMapping("MenuNo", "MenuNo"), New System.Data.Common.DataColumnMapping("IsProtected", "IsProtected"), New System.Data.Common.DataColumnMapping("ShowSearch", "ShowSearch"), New System.Data.Common.DataColumnMapping("ShowMenu", "ShowMenu"), New System.Data.Common.DataColumnMapping("ArticlelistHeader", "ArticlelistHeader"), New System.Data.Common.DataColumnMapping("Archive", "Archive"), New System.Data.Common.DataColumnMapping("Title", "Title"), New System.Data.Common.DataColumnMapping("Ingress", "Ingress"), New System.Data.Common.DataColumnMapping("Text", "Text"), New System.Data.Common.DataColumnMapping("Footer", "Footer"), New System.Data.Common.DataColumnMapping("HTMLContent", "HTMLContent")})})
        Me.daSection.UpdateCommand = Me.SqlUpdateCommand1
        '
        'SqlDeleteCommand1
        '
        Me.SqlDeleteCommand1.CommandText = "DELETE FROM SECTIONS WHERE (Id = @Original_Id) AND (Archive = @Original_Archive O" & _
        "R @Original_Archive IS NULL AND Archive IS NULL) AND (HTMLContent = @Original_HT" & _
        "MLContent OR @Original_HTMLContent IS NULL AND HTMLContent IS NULL) AND (IsProte" & _
        "cted = @Original_IsProtected OR @Original_IsProtected IS NULL AND IsProtected IS" & _
        " NULL) AND (Lastmodified = @Original_Lastmodified OR @Original_Lastmodified IS N" & _
        "ULL AND Lastmodified IS NULL) AND (MainGroup = @Original_MainGroup) AND (MenuNo " & _
        "= @Original_MenuNo OR @Original_MenuNo IS NULL AND MenuNo IS NULL) AND (Name = @" & _
        "Original_Name OR @Original_Name IS NULL AND Name IS NULL) AND (NumArticles = @Or" & _
        "iginal_NumArticles OR @Original_NumArticles IS NULL AND NumArticles IS NULL) AND" & _
        " (PageCount = @Original_PageCount OR @Original_PageCount IS NULL AND PageCount I" & _
        "S NULL) AND (PictureHeading = @Original_PictureHeading OR @Original_PictureHeadi" & _
        "ng IS NULL AND PictureHeading IS NULL) AND (PictureHeight = @Original_PictureHei" & _
        "ght OR @Original_PictureHeight IS NULL AND PictureHeight IS NULL) AND (PictureWi" & _
        "dth = @Original_PictureWidth OR @Original_PictureWidth IS NULL AND PictureWidth " & _
        "IS NULL) AND (ShowMenu = @Original_ShowMenu OR @Original_ShowMenu IS NULL AND Sh" & _
        "owMenu IS NULL) AND (ShowSearch = @Original_ShowSearch OR @Original_ShowSearch I" & _
        "S NULL AND ShowSearch IS NULL) AND (SiteCode = @Original_SiteCode OR @Original_S" & _
        "iteCode IS NULL AND SiteCode IS NULL) AND (SubGroup = @Original_SubGroup OR @Ori" & _
        "ginal_SubGroup IS NULL AND SubGroup IS NULL) AND (Title = @Original_Title OR @Or" & _
        "iginal_Title IS NULL AND Title IS NULL) AND (TitleGif = @Original_TitleGif OR @O" & _
        "riginal_TitleGif IS NULL AND TitleGif IS NULL)"
        Me.SqlDeleteCommand1.Connection = Me.dbNTB
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Archive", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Archive", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_HTMLContent", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "HTMLContent", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IsProtected", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IsProtected", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Lastmodified", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Lastmodified", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MainGroup", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MainGroup", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MenuNo", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MenuNo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Name", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Name", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumArticles", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumArticles", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PageCount", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PageCount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PictureHeading", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PictureHeading", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PictureHeight", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PictureHeight", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PictureWidth", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PictureWidth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ShowMenu", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ShowMenu", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ShowSearch", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ShowSearch", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SiteCode", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SiteCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubGroup", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubGroup", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Title", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Title", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TitleGif", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TitleGif", System.Data.DataRowVersion.Original, Nothing))
        '
        'dbNTB
        '
        Me.dbNTB.ConnectionString = "packet size=4096;user id=sa;data source=melbourne;persist security info=True;init" & _
        "ial catalog=ntb_web;password=boller&Brus"
        '
        'SqlInsertCommand1
        '
        Me.SqlInsertCommand1.CommandText = "INSERT INTO SECTIONS(SiteCode, MainGroup, SubGroup, Name, Lastmodified, Content, " & _
        "TitleGif, Picture, PictureHTML, PictureWidth, PictureHeight, PictureHeading, Pic" & _
        "tureText, NumArticles, PageCount, MenuNo, IsProtected, ShowSearch, ShowMenu, Art" & _
        "iclelistHeader, Archive, Title, Ingress, Text, Footer, HTMLContent) VALUES (@Sit" & _
        "eCode, @MainGroup, @SubGroup, @Name, @Lastmodified, @Content, @TitleGif, @Pictur" & _
        "e, @PictureHTML, @PictureWidth, @PictureHeight, @PictureHeading, @PictureText, @" & _
        "NumArticles, @PageCount, @MenuNo, @IsProtected, @ShowSearch, @ShowMenu, @Article" & _
        "listHeader, @Archive, @Title, @Ingress, @Text, @Footer, @HTMLContent); SELECT Id" & _
        ", SiteCode, MainGroup, SubGroup, Name, Lastmodified, Content, TitleGif, Picture," & _
        " PictureHTML, PictureWidth, PictureHeight, PictureHeading, PictureText, NumArtic" & _
        "les, PageCount, MenuNo, IsProtected, ShowSearch, ShowMenu, ArticlelistHeader, Ar" & _
        "chive, Title, Ingress, Text, Footer, HTMLContent FROM SECTIONS WHERE (Id = @@IDE" & _
        "NTITY)"
        Me.SqlInsertCommand1.Connection = Me.dbNTB
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SiteCode", System.Data.SqlDbType.VarChar, 3, "SiteCode"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MainGroup", System.Data.SqlDbType.Int, 4, "MainGroup"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubGroup", System.Data.SqlDbType.Int, 4, "SubGroup"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Name", System.Data.SqlDbType.VarChar, 100, "Name"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Lastmodified", System.Data.SqlDbType.DateTime, 8, "Lastmodified"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Content", System.Data.SqlDbType.VarChar, 2147483647, "Content"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TitleGif", System.Data.SqlDbType.VarChar, 255, "TitleGif"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Picture", System.Data.SqlDbType.VarBinary, 2147483647, "Picture"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PictureHTML", System.Data.SqlDbType.VarChar, 2147483647, "PictureHTML"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PictureWidth", System.Data.SqlDbType.Int, 4, "PictureWidth"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PictureHeight", System.Data.SqlDbType.Int, 4, "PictureHeight"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PictureHeading", System.Data.SqlDbType.VarChar, 255, "PictureHeading"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PictureText", System.Data.SqlDbType.VarChar, 2147483647, "PictureText"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumArticles", System.Data.SqlDbType.Int, 4, "NumArticles"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PageCount", System.Data.SqlDbType.TinyInt, 1, "PageCount"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MenuNo", System.Data.SqlDbType.TinyInt, 1, "MenuNo"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IsProtected", System.Data.SqlDbType.TinyInt, 1, "IsProtected"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ShowSearch", System.Data.SqlDbType.TinyInt, 1, "ShowSearch"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ShowMenu", System.Data.SqlDbType.TinyInt, 1, "ShowMenu"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ArticlelistHeader", System.Data.SqlDbType.VarChar, 2147483647, "ArticlelistHeader"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Archive", System.Data.SqlDbType.Int, 4, "Archive"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Title", System.Data.SqlDbType.VarChar, 255, "Title"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Ingress", System.Data.SqlDbType.VarChar, 2147483647, "Ingress"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Text", System.Data.SqlDbType.VarChar, 2147483647, "Text"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Footer", System.Data.SqlDbType.VarChar, 2147483647, "Footer"))
        Me.SqlInsertCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@HTMLContent", System.Data.SqlDbType.TinyInt, 1, "HTMLContent"))
        '
        'SqlSelectCommand1
        '
        Me.SqlSelectCommand1.CommandText = "SELECT Id, SiteCode, MainGroup, SubGroup, Name, Lastmodified, Content, TitleGif, " & _
        "Picture, PictureHTML, PictureWidth, PictureHeight, PictureHeading, PictureText, " & _
        "NumArticles, PageCount, MenuNo, IsProtected, ShowSearch, ShowMenu, ArticlelistHe" & _
        "ader, Archive, Title, Ingress, Text, Footer, HTMLContent FROM SECTIONS where Id " & _
        "= @id"
        Me.SqlSelectCommand1.Connection = Me.dbNTB
        Me.SqlSelectCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@id", System.Data.SqlDbType.Int, 4, "Id"))
        '
        'SqlUpdateCommand1
        '
        Me.SqlUpdateCommand1.CommandText = "UPDATE SECTIONS SET SiteCode = @SiteCode, MainGroup = @MainGroup, SubGroup = @Sub" & _
        "Group, Name = @Name, Lastmodified = @Lastmodified, Content = @Content, TitleGif " & _
        "= @TitleGif, Picture = @Picture, PictureHTML = @PictureHTML, PictureWidth = @Pic" & _
        "tureWidth, PictureHeight = @PictureHeight, PictureHeading = @PictureHeading, Pic" & _
        "tureText = @PictureText, NumArticles = @NumArticles, PageCount = @PageCount, Men" & _
        "uNo = @MenuNo, IsProtected = @IsProtected, ShowSearch = @ShowSearch, ShowMenu = " & _
        "@ShowMenu, ArticlelistHeader = @ArticlelistHeader, Archive = @Archive, Title = @" & _
        "Title, Ingress = @Ingress, Text = @Text, Footer = @Footer, HTMLContent = @HTMLCo" & _
        "ntent WHERE (Id = @Original_Id) AND (Archive = @Original_Archive OR @Original_Ar" & _
        "chive IS NULL AND Archive IS NULL) AND (HTMLContent = @Original_HTMLContent OR @" & _
        "Original_HTMLContent IS NULL AND HTMLContent IS NULL) AND (IsProtected = @Origin" & _
        "al_IsProtected OR @Original_IsProtected IS NULL AND IsProtected IS NULL) AND (La" & _
        "stmodified = @Original_Lastmodified OR @Original_Lastmodified IS NULL AND Lastmo" & _
        "dified IS NULL) AND (MainGroup = @Original_MainGroup) AND (MenuNo = @Original_Me" & _
        "nuNo OR @Original_MenuNo IS NULL AND MenuNo IS NULL) AND (Name = @Original_Name " & _
        "OR @Original_Name IS NULL AND Name IS NULL) AND (NumArticles = @Original_NumArti" & _
        "cles OR @Original_NumArticles IS NULL AND NumArticles IS NULL) AND (PageCount = " & _
        "@Original_PageCount OR @Original_PageCount IS NULL AND PageCount IS NULL) AND (P" & _
        "ictureHeading = @Original_PictureHeading OR @Original_PictureHeading IS NULL AND" & _
        " PictureHeading IS NULL) AND (PictureHeight = @Original_PictureHeight OR @Origin" & _
        "al_PictureHeight IS NULL AND PictureHeight IS NULL) AND (PictureWidth = @Origina" & _
        "l_PictureWidth OR @Original_PictureWidth IS NULL AND PictureWidth IS NULL) AND (" & _
        "ShowMenu = @Original_ShowMenu OR @Original_ShowMenu IS NULL AND ShowMenu IS NULL" & _
        ") AND (ShowSearch = @Original_ShowSearch OR @Original_ShowSearch IS NULL AND Sho" & _
        "wSearch IS NULL) AND (SiteCode = @Original_SiteCode OR @Original_SiteCode IS NUL" & _
        "L AND SiteCode IS NULL) AND (SubGroup = @Original_SubGroup OR @Original_SubGroup" & _
        " IS NULL AND SubGroup IS NULL) AND (Title = @Original_Title OR @Original_Title I" & _
        "S NULL AND Title IS NULL) AND (TitleGif = @Original_TitleGif OR @Original_TitleG" & _
        "if IS NULL AND TitleGif IS NULL); SELECT Id, SiteCode, MainGroup, SubGroup, Name" & _
        ", Lastmodified, Content, TitleGif, Picture, PictureHTML, PictureWidth, PictureHe" & _
        "ight, PictureHeading, PictureText, NumArticles, PageCount, MenuNo, IsProtected, " & _
        "ShowSearch, ShowMenu, ArticlelistHeader, Archive, Title, Ingress, Text, Footer, " & _
        "HTMLContent FROM SECTIONS WHERE (Id = @Id)"
        Me.SqlUpdateCommand1.Connection = Me.dbNTB
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SiteCode", System.Data.SqlDbType.VarChar, 3, "SiteCode"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MainGroup", System.Data.SqlDbType.Int, 4, "MainGroup"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@SubGroup", System.Data.SqlDbType.Int, 4, "SubGroup"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Name", System.Data.SqlDbType.VarChar, 100, "Name"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Lastmodified", System.Data.SqlDbType.DateTime, 8, "Lastmodified"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Content", System.Data.SqlDbType.VarChar, 2147483647, "Content"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@TitleGif", System.Data.SqlDbType.VarChar, 255, "TitleGif"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Picture", System.Data.SqlDbType.VarBinary, 2147483647, "Picture"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PictureHTML", System.Data.SqlDbType.VarChar, 2147483647, "PictureHTML"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PictureWidth", System.Data.SqlDbType.Int, 4, "PictureWidth"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PictureHeight", System.Data.SqlDbType.Int, 4, "PictureHeight"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PictureHeading", System.Data.SqlDbType.VarChar, 255, "PictureHeading"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PictureText", System.Data.SqlDbType.VarChar, 2147483647, "PictureText"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@NumArticles", System.Data.SqlDbType.Int, 4, "NumArticles"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PageCount", System.Data.SqlDbType.TinyInt, 1, "PageCount"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MenuNo", System.Data.SqlDbType.TinyInt, 1, "MenuNo"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IsProtected", System.Data.SqlDbType.TinyInt, 1, "IsProtected"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ShowSearch", System.Data.SqlDbType.TinyInt, 1, "ShowSearch"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ShowMenu", System.Data.SqlDbType.TinyInt, 1, "ShowMenu"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ArticlelistHeader", System.Data.SqlDbType.VarChar, 2147483647, "ArticlelistHeader"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Archive", System.Data.SqlDbType.Int, 4, "Archive"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Title", System.Data.SqlDbType.VarChar, 255, "Title"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Ingress", System.Data.SqlDbType.VarChar, 2147483647, "Ingress"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Text", System.Data.SqlDbType.VarChar, 2147483647, "Text"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Footer", System.Data.SqlDbType.VarChar, 2147483647, "Footer"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@HTMLContent", System.Data.SqlDbType.TinyInt, 1, "HTMLContent"))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Archive", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Archive", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_HTMLContent", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "HTMLContent", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IsProtected", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IsProtected", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Lastmodified", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Lastmodified", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MainGroup", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MainGroup", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MenuNo", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MenuNo", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Name", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Name", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_NumArticles", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "NumArticles", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PageCount", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PageCount", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PictureHeading", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PictureHeading", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PictureHeight", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PictureHeight", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_PictureWidth", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "PictureWidth", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ShowMenu", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ShowMenu", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_ShowSearch", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "ShowSearch", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SiteCode", System.Data.SqlDbType.VarChar, 3, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SiteCode", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_SubGroup", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "SubGroup", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Title", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Title", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_TitleGif", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "TitleGif", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.Int, 4, "Id"))
        '
        'daUser
        '
        Me.daUser.DeleteCommand = Me.SqlDeleteCommand2
        Me.daUser.InsertCommand = Me.SqlCommand1
        Me.daUser.SelectCommand = Me.SqlSelectCommand2
        Me.daUser.TableMappings.AddRange(New System.Data.Common.DataTableMapping() {New System.Data.Common.DataTableMapping("Table", "USERS", New System.Data.Common.DataColumnMapping() {New System.Data.Common.DataColumnMapping("Id", "Id"), New System.Data.Common.DataColumnMapping("UserId", "UserId"), New System.Data.Common.DataColumnMapping("Name", "Name"), New System.Data.Common.DataColumnMapping("Password", "Password"), New System.Data.Common.DataColumnMapping("MainGroups", "MainGroups"), New System.Data.Common.DataColumnMapping("MainCats", "MainCats"), New System.Data.Common.DataColumnMapping("LastLoggedIn", "LastLoggedIn"), New System.Data.Common.DataColumnMapping("IPRange", "IPRange"), New System.Data.Common.DataColumnMapping("Invoice", "Invoice"), New System.Data.Common.DataColumnMapping("IsAdmin", "IsAdmin"), New System.Data.Common.DataColumnMapping("Email", "Email"), New System.Data.Common.DataColumnMapping("Active", "Active"), New System.Data.Common.DataColumnMapping("Note", "Note")})})
        Me.daUser.UpdateCommand = Me.SqlUpdateCommand2
        '
        'SqlDeleteCommand2
        '
        Me.SqlDeleteCommand2.CommandText = "DELETE FROM USERS WHERE (Id = @Original_Id) AND (Active = @Original_Active OR @Or" & _
        "iginal_Active IS NULL AND Active IS NULL) AND (Email = @Original_Email OR @Origi" & _
        "nal_Email IS NULL AND Email IS NULL) AND (Invoice = @Original_Invoice OR @Origin" & _
        "al_Invoice IS NULL AND Invoice IS NULL) AND (IsAdmin = @Original_IsAdmin OR @Ori" & _
        "ginal_IsAdmin IS NULL AND IsAdmin IS NULL) AND (LastLoggedIn = @Original_LastLog" & _
        "gedIn OR @Original_LastLoggedIn IS NULL AND LastLoggedIn IS NULL) AND (MainCats " & _
        "= @Original_MainCats OR @Original_MainCats IS NULL AND MainCats IS NULL) AND (Ma" & _
        "inGroups = @Original_MainGroups OR @Original_MainGroups IS NULL AND MainGroups I" & _
        "S NULL) AND (Name = @Original_Name OR @Original_Name IS NULL AND Name IS NULL) A" & _
        "ND (Password = @Original_Password OR @Original_Password IS NULL AND Password IS " & _
        "NULL) AND (UserId = @Original_UserId OR @Original_UserId IS NULL AND UserId IS N" & _
        "ULL)"
        Me.SqlDeleteCommand2.Connection = Me.dbNTB
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Active", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Active", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Email", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Email", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Invoice", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Invoice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IsAdmin", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IsAdmin", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LastLoggedIn", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LastLoggedIn", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MainCats", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MainCats", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MainGroups", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MainGroups", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Name", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Name", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Password", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Password", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlDeleteCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UserId", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UserId", System.Data.DataRowVersion.Original, Nothing))
        '
        'SqlCommand1
        '
        Me.SqlCommand1.CommandText = "INSERT INTO USERS (UserId, Name, Password, MainGroups, MainCats, Invoice, IsAdmin" & _
        ", Email, Active, Phone, CreatedBy, Address, IPRange, Note, PasswdIPCheck, IsDate" & _
        "Limited, Expires, ChangePwdInterval, WarningSent, PwdChangedDate) VALUES (@UserI" & _
        "d, @Name, @Password, @Maingroups, @Maincats, @Invoice, @IsAdmin, @Email, @Active" & _
        ", @Phone, @CreatedBy, @Address, @IPRange, @Note, @PwdIP, @Expire, @ExpireDate, @" & _
        "ChangePwdInterval, @WarningSent, @PwdChangedDate)"
        Me.SqlCommand1.Connection = Me.dbNTB
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UserId", System.Data.SqlDbType.VarChar, 20, "UserId"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Name", System.Data.SqlDbType.VarChar, 100, "Name"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Password", System.Data.SqlDbType.VarChar, 20, "Password"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Maingroups", System.Data.SqlDbType.VarChar, 255, "MainGroups"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Maincats", System.Data.SqlDbType.VarChar, 255, "MainCats"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Invoice", System.Data.SqlDbType.TinyInt, 1, "Invoice"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IsAdmin", System.Data.SqlDbType.TinyInt, 1, "IsAdmin"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Email", System.Data.SqlDbType.VarChar, 100, "Email"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Active", System.Data.SqlDbType.TinyInt, 1, "Active"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Phone", System.Data.SqlDbType.VarChar, 20, "Phone"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CreatedBy", System.Data.SqlDbType.VarChar, 100, "CreatedBy"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Address", System.Data.SqlDbType.VarChar, 2147483647, "Address"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IPRange", System.Data.SqlDbType.VarChar, 2147483647, "IPRange"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Note", System.Data.SqlDbType.VarChar, 2147483647, "Note"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PwdIP", System.Data.SqlDbType.TinyInt, 1, "PasswdIPCheck"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Expire", System.Data.SqlDbType.TinyInt, 1, "IsDateLimited"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ExpireDate", System.Data.SqlDbType.DateTime, 8, "Expires"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChangePwdInterval", System.Data.SqlDbType.TinyInt, 1, "ChangePwdInterval"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@WarningSent", System.Data.SqlDbType.TinyInt, 1, "WarningSent"))
        Me.SqlCommand1.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PwdChangedDate", System.Data.SqlDbType.DateTime, 8, "PwdChangedDate"))
        '
        'SqlSelectCommand2
        '
        Me.SqlSelectCommand2.CommandText = "SELECT Id, UserId, Name, Password, MainGroups, MainCats, LastLoggedIn, IPRange, I" & _
        "nvoice, IsAdmin, Email, Active, Note, Phone, Address, CreatedBy, Created, Passwd" & _
        "IPCheck, IsDateLimited, Expires, ChangePwdInterval, PwdChangedDate, WarningSent " & _
        "FROM USERS WHERE (Id = @id)"
        Me.SqlSelectCommand2.Connection = Me.dbNTB
        Me.SqlSelectCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@id", System.Data.SqlDbType.Int, 4, "Id"))
        '
        'SqlUpdateCommand2
        '
        Me.SqlUpdateCommand2.CommandText = "UPDATE USERS SET UserId = @UserId, Name = @Name, Password = @Password, MainGroups" & _
        " = @MainGroups, MainCats = @MainCats, LastLoggedIn = @LastLoggedIn, IPRange = @I" & _
        "PRange, Invoice = @Invoice, IsAdmin = @IsAdmin, Email = @Email, Active = @Active" & _
        ", Note = @Note, Created = @Created, CreatedBy = @CreatedBy, Address = @Address, " & _
        "Phone = @Phone, PasswdIpCheck = @PwdIp, IsDateLImited = @Expire, Expires = @Expi" & _
        "reDate, ChangePwdInterval = @ChangePwdInterval, PwdChangedDate = @PwdChangedDate" & _
        ", WarningSent = @WarningSent WHERE (Id = @Original_Id) AND (Active = @Original_A" & _
        "ctive OR @Original_Active IS NULL AND Active IS NULL) AND (Email = @Original_Ema" & _
        "il OR @Original_Email IS NULL AND Email IS NULL) AND (Invoice = @Original_Invoic" & _
        "e OR @Original_Invoice IS NULL AND Invoice IS NULL) AND (IsAdmin = @Original_IsA" & _
        "dmin OR @Original_IsAdmin IS NULL AND IsAdmin IS NULL) AND (LastLoggedIn = @Orig" & _
        "inal_LastLoggedIn OR @Original_LastLoggedIn IS NULL AND LastLoggedIn IS NULL) AN" & _
        "D (MainCats = @Original_MainCats OR @Original_MainCats IS NULL AND MainCats IS N" & _
        "ULL) AND (MainGroups = @Original_MainGroups OR @Original_MainGroups IS NULL AND " & _
        "MainGroups IS NULL) AND (Name = @Original_Name OR @Original_Name IS NULL AND Nam" & _
        "e IS NULL) AND (Password = @Original_Password OR @Original_Password IS NULL AND " & _
        "Password IS NULL) AND (UserId = @Original_UserId OR @Original_UserId IS NULL AND" & _
        " UserId IS NULL); SELECT Id, UserId, Name, Password, MainGroups, MainCats, LastL" & _
        "oggedIn, IPRange, Invoice, IsAdmin, Email, Active, Note FROM USERS WHERE (Id = @" & _
        "Id)"
        Me.SqlUpdateCommand2.Connection = Me.dbNTB
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@UserId", System.Data.SqlDbType.VarChar, 20, "UserId"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Name", System.Data.SqlDbType.VarChar, 100, "Name"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Password", System.Data.SqlDbType.VarChar, 20, "Password"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MainGroups", System.Data.SqlDbType.VarChar, 255, "MainGroups"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@MainCats", System.Data.SqlDbType.VarChar, 255, "MainCats"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@LastLoggedIn", System.Data.SqlDbType.DateTime, 8, "LastLoggedIn"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IPRange", System.Data.SqlDbType.VarChar, 2147483647, "IPRange"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Invoice", System.Data.SqlDbType.TinyInt, 1, "Invoice"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@IsAdmin", System.Data.SqlDbType.TinyInt, 1, "IsAdmin"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Email", System.Data.SqlDbType.VarChar, 100, "Email"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Active", System.Data.SqlDbType.TinyInt, 1, "Active"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Note", System.Data.SqlDbType.VarChar, 2147483647, "Note"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Created", System.Data.SqlDbType.DateTime, 8, "Created"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@CreatedBy", System.Data.SqlDbType.VarChar, 100, "CreatedBy"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Address", System.Data.SqlDbType.VarChar, 2147483647, "Address"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Phone", System.Data.SqlDbType.VarChar, 20, "Phone"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PwdIp", System.Data.SqlDbType.TinyInt, 1, "PasswdIPCheck"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Expire", System.Data.SqlDbType.TinyInt, 1, "IsDateLimited"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ExpireDate", System.Data.SqlDbType.DateTime, 8, "Expires"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@ChangePwdInterval", System.Data.SqlDbType.TinyInt, 1, "ChangePwdInterval"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@PwdChangedDate", System.Data.SqlDbType.DateTime, 8, "PwdChangedDate"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@WarningSent", System.Data.SqlDbType.TinyInt, 1, "WarningSent"))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Id", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Id", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Active", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Active", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Email", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Email", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Invoice", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Invoice", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_IsAdmin", System.Data.SqlDbType.TinyInt, 1, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "IsAdmin", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_LastLoggedIn", System.Data.SqlDbType.DateTime, 8, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "LastLoggedIn", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MainCats", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MainCats", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_MainGroups", System.Data.SqlDbType.VarChar, 255, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "MainGroups", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Name", System.Data.SqlDbType.VarChar, 100, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Name", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_Password", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "Password", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Original_UserId", System.Data.SqlDbType.VarChar, 20, System.Data.ParameterDirection.Input, False, CType(0, Byte), CType(0, Byte), "UserId", System.Data.DataRowVersion.Original, Nothing))
        Me.SqlUpdateCommand2.Parameters.Add(New System.Data.SqlClient.SqlParameter("@Id", System.Data.SqlDbType.Int, 4, "Id"))
        '
        'MainForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(848, 630)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.sbMain)
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NTB Edit"
        CType(Me.sbProg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sbUser, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sbMisc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.tbSection.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.pSection.ResumeLayout(False)
        Me.TabControl2.ResumeLayout(False)
        Me.tpEgenskaper.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.tpInnhold.ResumeLayout(False)
        Me.tpBilde.ResumeLayout(False)
        Me.tpHTML.ResumeLayout(False)
        CType(Me.AxWebBrowser1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.tbStats.ResumeLayout(False)
        Me.tcStats.ResumeLayout(False)
        Me.tpDownload.ResumeLayout(False)
        CType(Me.dgDLStats, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel7.ResumeLayout(False)
        Me.Panel11.ResumeLayout(False)
        Me.tpStats.ResumeLayout(False)
        CType(Me.dgStats, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel8.ResumeLayout(False)
        Me.Panel12.ResumeLayout(False)
        Me.tpUserStats.ResumeLayout(False)
        CType(Me.dgUserStats, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel13.ResumeLayout(False)
        Me.Panel14.ResumeLayout(False)
        Me.tbUser.ResumeLayout(False)
        Me.Panel9.ResumeLayout(False)
        Me.pUser.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.tbArticle.ResumeLayout(False)
        CType(Me.dgArticles, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel10.ResumeLayout(False)
        Me.tbMenu.ResumeLayout(False)
        CType(Me.web, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    ' Sjekker om brukerid allerede er i bruk
    Public Function SjekkUser(ByVal userid As String) As Boolean
        Dim cd As New SqlCommand("select UserId from USERS where UserId = '" & userid & "'", dbNTB)
        dbNTB.Open()
        SjekkUser = (cd.ExecuteScalar = userid)
        dbNTB.Close()
    End Function

    ' Sjekker om seksjonsid allerede er i bruk
    Public Function SjekkSection(ByVal section As String) As Boolean
        Dim cd As New SqlCommand("select Name from SECTIONS where Name = '" & section & "'", dbNTB)
        dbNTB.Open()
        SjekkSection = (cd.ExecuteScalar = section)
        dbNTB.Close()
    End Function

    Public Function BitCommaText(ByVal value As Long) As String
        Dim no1, no2 As Long
        Dim res As String
        no2 = value
        While (no2 > 0)
            no1 = 1
            While no1 <= no2
                no1 = no1 * 2
            End While
            If res = "" Then res = CStr(no1 / 2) Else res = res + "," + CStr(no1 / 2)
            no2 = no2 - no1 / 2
        End While
        BitCommaText = res
    End Function

    ' Selecterer items i en listbox basert p� values som enten er kommaseparert eller "bit-summert"
    Private Sub MakeSelected(ByVal lb As ListBox, ByVal values As String, Optional ByVal iscommadelimited As Boolean = True)
        Dim arr As Array
        Dim i As Integer
        Dim items As String

        lb.ClearSelected()
        lb.SelectedIndex = -1
        If iscommadelimited Then
            items = values
        Else
            items = BitCommaText(CLng(values))
        End If
        arr = Split(items, ",")
        Dim item = New ListView
        i = 0
        For i = 0 To lb.Items.Count - 1
            item = lb.Items(i)
            If arr.IndexOf(arr, CStr(CType(CType(item, System.Data.DataRowView).Row, System.Data.DataRow).ItemArray(1))) <> -1 Then
                lb.SelectedIndex = i
            End If
        Next
    End Sub

    ' Henter verdiene til selekterte items i en listboks
    Public Function GetSelected(ByVal lb As ListBox) As String
        Dim i As Integer
        Dim items As String

        Dim item = New ListView
        i = 0
        For i = 0 To lb.SelectedItems.Count - 1
            item = lb.SelectedItems(i)
            items += CStr(CType(CType(item, System.Data.DataRowView).Row, System.Data.DataRow).ItemArray(1)) + ","
        Next
        If items = "" Then items = "-1"
        GetSelected = items
    End Function

    ' Fyller listbox eller combobox med Menyer fra DB
    Private Sub PopulateMenus(ByVal cb)
        Dim da As New SqlDataAdapter("SELECT Id, Name FROM MENUES Order by Id", dbNTB)
        Dim ds As New DataSet

        da.Fill(ds, "Menues")
        cb.DisplayMember = "Name"
        cb.ValueMember = "Id"
        cb.DataSource = ds.Tables("Menues")
    End Sub

    ' Fyller listbox eller combobox med kategorier fra DB
    Private Sub PopulateCategories(ByVal cb, ByVal id)
        Dim da As New SqlDataAdapter("SELECT Descriptivename, Category FROM BITNAMES where TypeOfName = " & id & " and inactive = 0 order by sorter, Descriptivename", dbNTB)
        Dim ds As New DataSet

        da.Fill(ds, "Categories" & id)
        cb.DisplayMember = "Descriptivename"
        cb.ValueMember = "Category"
        cb.DataSource = ds.Tables("Categories" & id)
    End Sub

    ' Fyller listbox eller combobox med seksjoner fra DB
    Private Sub PopulateSections(ByVal cb, Optional ByVal find = 0)
        Dim da As New SqlDataAdapter("SELECT Id, Name FROM SECTIONS Where SiteCode = '" & sitecode & "' Order by Name", dbNTB)
        Dim ds As New DataSet

        da.Fill(ds, "Sections")
        cb.DisplayMember = "Name"
        cb.ValueMember = "Id"
        cb.DataSource = ds.Tables("Sections")
        Try
            If find > 0 Then cb.SelectedValue = find
        Catch
        End Try
    End Sub

    ' Fyller listbox eller combobox med brukere fra DB
    Private Sub PopulateUsers(ByVal cb, Optional ByVal find = 0, Optional ByVal sg = 0)

        Dim query As String = "SELECT Id, UserId, Name, Active FROM USERS"

        If sg > 0 Then
            query &= " WHERE (MainGroups LIKE '%," & sg & ",%') OR (MainGroups LIKE '" & sg & ",%') OR (MainGroups = '-1')"
        End If

        query &= " Order by Name, UserId"

        Dim da As New SqlDataAdapter(query, dbNTB)
        Dim ds As New DataSet

        da.Fill(ds, "Users")

        'cb.DisplayMember = "UserId"
        'cb.ValueMember = "Id"
        'cb.DataSource = ds.Tables("Users")

        cb.datasource = Nothing
        cb.items.clear()
        userList.Clear()

        Dim row As DataRow
        For Each row In ds.Tables("Users").Rows
            Dim itm As usrItem = New usrItem
            itm.id = row("Id")
            itm.label = row("Name") & " ( " & row("UserId") & " ) "

            If row("Active") = 0 Then
                itm.label &= " -- INAKTIV"
            End If

            userList.Add(itm)
        Next

        'cb.Items.Clear()
        cb.DisplayMember = "label"
        cb.ValueMember = "id"
        cb.DataSource = userList

        Try
            If find > 0 Then cb.SelectedValue = find
        Catch
        End Try

    End Sub

    'Private Sub GetSection(ByVal sectionid As Integer)
    '    currsection = sectionid

    '    daSection.SelectCommand.Parameters.Item("@id").Value = sectionid
    '    dtSection = New DataTable
    '    daSection.Fill(dtSection)

    '    txtSection.DataBindings.Clear()
    '    rtxtContent.DataBindings.Clear()
    '    lLastmodified.DataBindings.Clear()
    '    txtNumarticles.DataBindings.Clear()
    '    txtPagecount.DataBindings.Clear()
    '    rtxtArticlelistheader.DataBindings.Clear()
    '    txtArchive.DataBindings.Clear()
    '    chkProtected.DataBindings.Clear()
    '    chkSearch.DataBindings.Clear()
    '    chkMenu.DataBindings.Clear()
    '    chkHTMLContent.DataBindings.Clear()
    '    lID.DataBindings.Clear()
    '    cbMenues.DataBindings.Clear()
    '    txtTitle.DataBindings.Clear()
    '    txtIngress.DataBindings.Clear()
    '    txtText.DataBindings.Clear()
    '    txtFooter.DataBindings.Clear()
    '    txtPictureText.DataBindings.Clear()
    '    txtPictureHeading.DataBindings.Clear()
    '    txtPictureHTML.DataBindings.Clear()
    '    txtTitleGif.DataBindings.Clear()

    '    If sectionid <> -1 And dtSection.Rows.Count = 1 Then
    '        Try
    '            txtSection.DataBindings.Add("Text", dtSection, "Name")
    '            rtxtContent.DataBindings.Add("Text", dtSection, "Content")
    '            lLastmodified.DataBindings.Add("Text", dtSection, "Lastmodified")
    '            txtNumarticles.DataBindings.Add("Text", dtSection, "NumArticles")
    '            txtPagecount.DataBindings.Add("Text", dtSection, "PageCount")
    '            rtxtArticlelistheader.DataBindings.Add("Text", dtSection, "Articlelistheader")
    '            txtArchive.DataBindings.Add("Text", dtSection, "Archive")
    '            chkProtected.DataBindings.Add("Checked", dtSection, "IsProtected")
    '            chkSearch.DataBindings.Add("Checked", dtSection, "ShowSearch")
    '            chkMenu.DataBindings.Add("Checked", dtSection, "ShowMenu")
    '            chkHTMLContent.DataBindings.Add("Checked", dtSection, "HTMLContent")
    '            lID.DataBindings.Add("Text", dtSection, "Id")
    '            'cbMenues.SelectedIndex = dtSection.Rows(0)("MenuNo")
    '            txtTitle.DataBindings.Add("Text", dtSection, "Title")
    '            txtIngress.DataBindings.Add("Text", dtSection, "Ingress")
    '            txtText.DataBindings.Add("Text", dtSection, "Text")
    '            txtFooter.DataBindings.Add("Text", dtSection, "Footer")
    '            txtPictureText.DataBindings.Add("Text", dtSection, "PictureText")
    '            txtPictureHeading.DataBindings.Add("Text", dtSection, "PictureHeading")
    '            txtPictureHTML.DataBindings.Add("Text", dtSection, "PictureHTML")
    '            txtTitleGif.DataBindings.Add("Text", dtSection, "TitleGif")
    '            chkHTMLContent_CheckStateChanged(Nothing, Nothing)
    '            imgSection.Image = Nothing
    '            Try
    '                If Not IsDBNull(dtSection.Rows(0)("Picture")) Then
    '                    Dim bytBlob() As Byte = dtSection.Rows(0)("Picture")
    '                    Dim strBlob As New MemoryStream(bytBlob)
    '                    If bytBlob.Length > 1 Then imgSection.Image = Image.FromStream(strBlob)
    '                    strBlob = Nothing
    '                    bytBlob = Nothing
    '                End If
    '            Catch e As Exception
    '                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
    '            End Try
    '            btnRemovePicture.Enabled = Not imgSection.Image Is Nothing
    '            MakeSelected(lbMaingroup, dtSection.Rows(0)("Maingroup"), False)
    '            MakeSelected(lbSubgroup, dtSection.Rows(0)("Subgroup"), False)
    '        Catch e As Exception
    '            MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
    '        End Try
    '    Else
    '        txtSection.Text = ""
    '        rtxtContent.Text = ""
    '        lLastmodified.Text = Now()
    '        txtNumarticles.Text = "0"
    '        txtPagecount.Text = "0"
    '        rtxtArticlelistheader.Text = ""
    '        txtArchive.Text = "0"
    '        chkProtected.Checked = False
    '        chkSearch.Checked = False
    '        chkMenu.Checked = True
    '        chkHTMLContent.Checked = False
    '        lID.Text = ""
    '        txtTitle.Text = ""
    '        txtIngress.Text = ""
    '        txtText.Text = ""
    '        txtFooter.Text = ""
    '        txtPictureHeading.Text = ""
    '        txtPictureText.Text = ""
    '        txtPictureHTML.Text = ""
    '        txtTitleGif.Text = ""
    '        imgSection.Image = Nothing
    '        cbMenues.SelectedIndex = 0
    '        lbMaingroup.SelectedIndex = -1
    '        lbSubgroup.SelectedIndex = -1
    '    End If
    '    txtName.Focus()
    'End Sub

    Private Sub GetSection(ByVal sectionid As Integer)
        currsection = sectionid

        daSection.SelectCommand.Parameters.Item("@id").Value = sectionid
        dtSection = New DataTable
        daSection.Fill(dtSection)

        If sectionid <> -1 And dtSection.Rows.Count = 1 Then
            Try
                txtSection.Text = dtSection.Rows(0)("Name") & ""
                rtxtContent.Text = dtSection.Rows(0)("Content") & ""
                lLastmodified.Text = dtSection.Rows(0)("Lastmodified") & ""
                txtNumarticles.Text = dtSection.Rows(0)("NumArticles") & ""
                txtPagecount.Text = dtSection.Rows(0)("PageCount") & ""
                rtxtArticlelistheader.Text = dtSection.Rows(0)("Articlelistheader") & ""
                txtArchive.Text = dtSection.Rows(0)("Archive") & ""
                chkProtected.Checked = dtSection.Rows(0)("IsProtected")
                chkSearch.Checked = dtSection.Rows(0)("ShowSearch")
                chkMenu.Checked = dtSection.Rows(0)("ShowMenu")
                chkHTMLContent.Checked = dtSection.Rows(0)("HTMLContent")

                lID.Text = dtSection.Rows(0)("Id") & ""
                txtTitle.Text = dtSection.Rows(0)("Title") & ""
                txtIngress.Text = dtSection.Rows(0)("Ingress") & ""
                txtText.Text = dtSection.Rows(0)("Text") & "" & ""
                txtFooter.Text = dtSection.Rows(0)("Footer") & ""
                txtPictureText.Text = dtSection.Rows(0)("PictureText") & ""
                txtPictureHeading.Text = dtSection.Rows(0)("PictureHeading") & ""
                txtPictureHTML.Text = dtSection.Rows(0)("PictureHTML") & ""
                txtTitleGif.Text = dtSection.Rows(0)("TitleGif") & ""

                chkHTMLContent_CheckStateChanged(Nothing, Nothing)

                imgSection.Image = Nothing
                Try
                    If Not IsDBNull(dtSection.Rows(0)("Picture")) Then
                        Dim bytBlob() As Byte = dtSection.Rows(0)("Picture")
                        Dim strBlob As New MemoryStream(bytBlob)
                        If bytBlob.Length > 1 Then imgSection.Image = Image.FromStream(strBlob)
                        strBlob = Nothing
                        bytBlob = Nothing
                    End If
                Catch e As Exception
                    MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
                End Try
                btnRemovePicture.Enabled = Not imgSection.Image Is Nothing
                MakeSelected(lbMaingroup, dtSection.Rows(0)("Maingroup"), False)
                MakeSelected(lbSubgroup, dtSection.Rows(0)("Subgroup"), False)
            Catch e As Exception
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            End Try
        Else
            txtSection.Text = ""
            rtxtContent.Text = ""
            lLastmodified.Text = Now()
            txtNumarticles.Text = "0"
            txtPagecount.Text = "0"
            rtxtArticlelistheader.Text = ""
            txtArchive.Text = "0"
            chkProtected.Checked = False
            chkSearch.Checked = False
            chkMenu.Checked = True
            chkHTMLContent.Checked = False
            lID.Text = ""
            txtTitle.Text = ""
            txtIngress.Text = ""
            txtText.Text = ""
            txtFooter.Text = ""
            txtPictureHeading.Text = ""
            txtPictureText.Text = ""
            txtPictureHTML.Text = ""
            txtTitleGif.Text = ""
            imgSection.Image = Nothing
            cbMenues.SelectedIndex = 0
            lbMaingroup.SelectedIndex = -1
            lbSubgroup.SelectedIndex = -1
        End If

        txtName.Focus()
    End Sub

    Private Sub GetUser(ByVal userid As Integer)
        curruser = userid

        daUser.SelectCommand.Parameters.Item("@id").Value = userid
        dtUser = New DataTable
        daUser.Fill(dtUser)

        lblId.DataBindings.Clear()
        txtUserId.DataBindings.Clear()
        txtPassword.DataBindings.Clear()
        txtName.DataBindings.Clear()
        txtEmail.DataBindings.Clear()
        'lbUserMaingroup.DataBindings.Clear()
        'lbUserMainCat.DataBindings.Clear()
        chkAdmin.DataBindings.Clear()
        chkInvoice.DataBindings.Clear()
        txtIPRange.DataBindings.Clear()
        chkActive.DataBindings.Clear()
        txtNote.DataBindings.Clear()
        chkIPPwd.DataBindings.Clear()

        Expire.DataBindings.Clear()
        ExpireDate.DataBindings.Clear()

        lblLastloggedin.DataBindings.Clear()
        lblCreated.DataBindings.Clear()

        createdBy.DataBindings.Clear()
        adresse.DataBindings.Clear()
        telefon.DataBindings.Clear()
        interval.DataBindings.Clear()


        If userid <> -1 And dtUser.Rows.Count = 1 Then
            Try
                lblId.DataBindings.Add("Text", dtUser, "Id")
                txtUserId.DataBindings.Add("Text", dtUser, "UserId")

                txtPassword.DataBindings.Add("Text", dtUser, "Password")
                OldPassword = txtPassword.Text
                PwdChangedDate = dtUser.Rows(0)("PwdChangedDate")

                txtName.DataBindings.Add("Text", dtUser, "Name")
                txtEmail.DataBindings.Add("Text", dtUser, "Email")
                MakeSelected(lbUserMaingroup, dtUser.Rows(0)("Maingroups"))
                MakeSelected(lbUserMainCat, dtUser.Rows(0)("MainCats"))
                chkAdmin.DataBindings.Add("Checked", dtUser, "IsAdmin")
                chkInvoice.DataBindings.Add("Checked", dtUser, "Invoice")
                txtIPRange.DataBindings.Add("Text", dtUser, "IPRange")
                chkActive.DataBindings.Add("Checked", dtUser, "Active")
                txtNote.DataBindings.Add("Text", dtUser, "Note")
                chkIPPwd.DataBindings.Add("Checked", dtUser, "PasswdIpCheck")

                Expire.DataBindings.Add("Checked", dtUser, "IsDateLimited")
                ExpireDate.DataBindings.Add("Value", dtUser, "Expires")
                interval.DataBindings.Add("Text", dtUser, "ChangePwdInterval")

                lblLastloggedin.DataBindings.Add("Text", dtUser, "Lastloggedin")

                Try
                    lblCreated.DataBindings.Add("Text", dtUser, "Created")
                    createdBy.DataBindings.Add("Text", dtUser, "CreatedBy")
                    adresse.DataBindings.Add("Text", dtUser, "Address")
                    telefon.DataBindings.Add("Text", dtUser, "Phone")
                Catch ex As Exception
                End Try

            Catch e As Exception
                MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            End Try
        Else
            lblId.Text = ""
            txtUserId.Text = ""
            txtPassword.Text = ""
            txtName.Text = ""
            txtEmail.Text = ""
            lbUserMaingroup.ClearSelected()
            lbUserMainCat.ClearSelected()
            chkAdmin.Checked = False
            chkInvoice.Checked = False
            txtIPRange.Text = ""
            chkActive.Checked = False
            txtNote.Text = ""
            chkIPPwd.Checked = False

            Expire.Checked = False
            ExpireDate.Value = Today

            OldPassword = ""
            PwdChangedDate = Today
            interval.Text = 0

            lblLastloggedin.Text = ""
            lblCreated.Text = ""

            createdBy.Text = ""
            adresse.Text = ""
            telefon.Text = ""

        End If
        txtUserId.Focus()
    End Sub

    Private Sub btnEditSection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditSection.Click, lbSections.DoubleClick
        PopulateMenus(cbMenues)
        PopulateCategories(lbMaingroup, 0)
        PopulateCategories(lbSubgroup, 1)
        pSection.Visible = True
        GetSection(lbSections.SelectedValue)
    End Sub

    Private Sub MainForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        sbUser.Text = "UserId: " & userid
        dbNTB.ConnectionString = conString
        PopulateCategories(cbCategories, 0)
        PopulateCategories(cbShowUserSG, 0)
        PopulateSections(lbSections)
        PopulateMenus(lbMenues)
        PopulateUsers(cbUsers)
        PopulateUsers(cbUsUser)
        PopulateUsers(lbUsers)
        dtFromDate.Value = Date.Now.AddDays(-30)
        dtFromDate2.Value = Date.Now.AddDays(-30)
        dtFromDate3.Value = Date.Now.AddDays(-30)
    End Sub

    Private Sub btnNewSection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewSection.Click
        PopulateMenus(cbMenues)
        PopulateCategories(lbMaingroup, 0)
        PopulateCategories(lbSubgroup, 1)
        pSection.Visible = True
        GetSection(-1)
        chkHTMLContent_CheckStateChanged(Nothing, Nothing)
    End Sub

    Private Sub btnSaveSection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveSection.Click
        SaveSection()
    End Sub

    Private Sub btnCancelSection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelSection.Click
        pSection.Visible = False
        currsection = -1
    End Sub

    Private Sub SaveSection()
        If Trim(txtSection.Text) = "" Then
            MessageBox.Show("Seksjonsnavn m� fylles inn!", "Feil", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Exit Sub
        End If
        If currsection = -1 And SjekkSection(txtSection.Text) Then
            MessageBox.Show("Seksjonen '" & txtSection.Text & "' finnes allerede. Velg et annet seksjonsnavn!", "Seksjon finnes allerede", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Exit Sub
        End If
        With dtSection
            If currsection = -1 Then
                Dim arow As DataRow = dtSection.NewRow()
                .Rows.Add(arow)
            End If
            .Rows(0).BeginEdit()
            Try
                If .Rows(0)("MainGroup").ToString = "" Then .Rows(0)("MainGroup") = -1
                If .Rows(0)("SubGroup").ToString = "" Then .Rows(0)("SubGroup") = -1
            Catch e As Exception
            End Try
            .Rows(0)("Id") = currsection
            .Rows(0)("SiteCode") = sitecode
            .Rows(0)("Name") = txtSection.Text
            .Rows(0)("NumArticles") = CInt(txtNumarticles.Text)
            .Rows(0)("LastModified") = Now()
            .Rows(0)("MenuNo") = cbMenues.SelectedValue
            .Rows(0)("Archive") = CInt(txtArchive.Text)
            .Rows(0)("PageCount") = CInt(txtPagecount.Text)
            .Rows(0)("Archive") = CInt(txtArchive.Text)
            .Rows(0)("ArticleListHeader") = rtxtArticlelistheader.Text
            .Rows(0)("Content") = rtxtContent.Text
            .Rows(0)("ShowSearch") = chkSearch.Checked
            .Rows(0)("ShowMenu") = chkMenu.Checked
            .Rows(0)("IsProtected") = chkProtected.Checked
            .Rows(0)("Title") = txtTitle.Text
            .Rows(0)("Ingress") = txtIngress.Text
            .Rows(0)("Text") = txtText.Text
            .Rows(0)("Footer") = txtFooter.Text
            .Rows(0)("TitleGif") = txtTitleGif.Text
            .Rows(0)("PictureHTML") = txtPictureHTML.Text
            .Rows(0)("HTMLContent") = chkHTMLContent.Checked
            .Rows(0)("Lastmodified") = Now
            .Rows(0)("PictureHeading") = txtPictureHeading.Text
            .Rows(0)("PictureText") = txtPictureText.Text
            If Not imgSection.Image Is Nothing Then
                .Rows(0)("PictureWidth") = imgSection.Image.Width
                .Rows(0)("PictureHeight") = imgSection.Image.Height
                Dim ms As New MemoryStream
                imgSection.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)
                Dim mydata(ms.Length) As Byte
                ms.Seek(0, SeekOrigin.Begin)
                ms.Read(mydata, 0, ms.Length)
                ms.Close()
                ms = Nothing
                .Rows(0)("Picture") = mydata
            Else
                Dim mydata(0) As Byte
                .Rows(0)("Picture") = mydata
            End If
            .Rows(0).EndEdit()
        End With
        daSection.Update(dtSection)
        PopulateSections(lbSections, lbSections.SelectedValue)
        pSection.Visible = False
        currsection = -1
    End Sub

    Private Sub SaveUser()
        If Trim(txtUserId.Text) = "" Or Trim(txtPassword.Text) = "" Then
            MessageBox.Show("BrukerId og passord m� fylles inn!", "Feil", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Exit Sub
        End If
        If curruser = -1 And SjekkUser(txtUserId.Text) Then
            MessageBox.Show("Brukeren '" & txtUserId.Text & "' finnes allerede. Velg en annen brukerid!", "Bruker finnes allerede", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Exit Sub
        End If
        With dtUser
            If curruser = -1 Then
                Dim arow As DataRow = dtUser.NewRow()
                .Rows.Add(arow)
            End If
            .Rows(0).BeginEdit()
            .Rows(0)("Id") = curruser
            .Rows(0)("UserId") = txtUserId.Text
            .Rows(0)("Name") = txtName.Text
            .Rows(0)("Password") = txtPassword.Text

            Dim s As String = GetSelected(lbUserMaingroup)
            If s <> "-1" Then
                .Rows(0)("MainGroups") = s
            Else
                .Rows(0)("MainGroups") = 0
            End If

            .Rows(0)("MainCats") = GetSelected(lbUserMainCat)
            'If GetSelected(lbUserMaingroup) <> "-1" Then
            '    .Rows(0)("MainCats") = GetSelected(lbUserMainCat)
            'Else
            '    .Rows(0)("MainCats") = 0
            'End If

            .Rows(0)("IPRange") = txtIPRange.Text
            .Rows(0)("Invoice") = chkInvoice.Checked
            .Rows(0)("IsAdmin") = chkAdmin.Checked
            .Rows(0)("Email") = txtEmail.Text
            .Rows(0)("Active") = chkActive.Checked
            .Rows(0)("Note") = txtNote.Text
            .Rows(0)("PasswdIpCheck") = chkIPPwd.Checked

            .Rows(0)("IsDateLimited") = Expire.Checked
            .Rows(0)("Expires") = ExpireDate.Value

            .Rows(0)("ChangePwdInterval") = interval.Text

            If OldPassword <> txtPassword.Text Then
                PwdChangedDate = Today
                .Rows(0)("WarningSent") = False
            End If

            .Rows(0)("PwdChangedDate") = PwdChangedDate
            .Rows(0)("Address") = adresse.Text
            .Rows(0)("Phone") = telefon.Text
            .Rows(0)("CreatedBy") = createdBy.Text

            .Rows(0).EndEdit()
        End With
        daUser.Update(dtUser)

        If onlySG.Checked Then
            PopulateUsers(lbUsers, lbUsers.SelectedValue, cbShowUserSG.SelectedValue)
        Else
            PopulateUsers(lbUsers, lbUsers.SelectedValue)
        End If

        pUser.Visible = False
    End Sub

    Private Sub btnSaveCloseSection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        SaveSection()
    End Sub

    Private Sub btnFindSection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFindSection.Click
        lbSections.SelectedIndex = lbSections.FindString(txtSearchSection.Text)
    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshSection.Click
        PopulateSections(lbSections, lbSections.SelectedValue)
    End Sub

    Private Sub btnHTML_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHTMLSection.Click
        Dim objStreamWriter As StreamWriter

        objStreamWriter = File.CreateText("c:\test.html")
        objStreamWriter.Write(rtxtContent.Text)
        objStreamWriter.Close()

        If CType(sender, Button).Text = "&HTML" Then

            web.Navigate2("c:\test.html")
            CType(sender, Button).Text = "&Tekst"
            web.Visible = True
            rtxtContent.Visible = False
        Else
            web.Visible = False
            rtxtContent.Visible = True
            CType(sender, Button).Text = "&HTML"
        End If

    End Sub

    Private Sub btnShowDLStats_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShowDLStats.Click
        Dim da As New SqlDataAdapter("_GetDLStats", dbNTB)
        Dim ds As New DataSet
        Dim uid As Integer

        If rbUser.Checked Then uid = cbUsers.SelectedValue Else uid = -1
        da.SelectCommand.CommandType = CommandType.StoredProcedure

        If AppSettings("Invoice") = 1 Then
            da.SelectCommand.Parameters.Add(New SqlParameter("@invoice", SqlDbType.Int, 4))
            da.SelectCommand.Parameters.Item("@invoice").Value = AppSettings("Invoice")
        End If

        da.SelectCommand.Parameters.Add(New SqlParameter("@maingroup", SqlDbType.VarChar, 50))
        da.SelectCommand.Parameters.Item("@maingroup").Value = AppSettings("DLGroups")

        da.SelectCommand.Parameters.Add(New SqlParameter("@userid", SqlDbType.Int, 4))
        da.SelectCommand.Parameters.Item("@userid").Value = uid

        da.SelectCommand.Parameters.Add(New SqlParameter("@fromdate", SqlDbType.VarChar, 8))
        da.SelectCommand.Parameters.Item("@fromdate").Value = Format(dtFromDate.Value, dtFromDate.CustomFormat)

        da.SelectCommand.Parameters.Add(New SqlParameter("@todate", SqlDbType.VarChar, 8))
        da.SelectCommand.Parameters.Item("@todate").Value = Format(dtToDate.Value.AddDays(1), dtFromDate.CustomFormat)

        Try
            da.Fill(ds, "DLStats")
        Catch ex As Exception
        End Try

        dgDLStats.DataSource = ds.Tables("DLStats")
    End Sub

    Private Sub btnShowPageStats_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShowPageStats.Click
        Dim da As New SqlDataAdapter("_GetStats", dbNTB)
        Dim ds As New DataSet

        da.SelectCommand.CommandType = CommandType.StoredProcedure

        da.SelectCommand.Parameters.Add(New SqlParameter("@fromdate", SqlDbType.VarChar, 8))
        da.SelectCommand.Parameters.Item("@fromdate").Value = Format(dtFromDate2.Value, dtFromDate2.CustomFormat)
        da.SelectCommand.Parameters.Add(New SqlParameter("@todate", SqlDbType.VarChar, 8))
        da.SelectCommand.Parameters.Item("@todate").Value = Format(dtToDate2.Value.AddDays(1), dtToDate2.CustomFormat)

        da.SelectCommand.Parameters.Add(New SqlParameter("@sitecode", SqlDbType.VarChar, 3))
        da.SelectCommand.Parameters.Item("@sitecode").Value = sitecode
        da.Fill(ds, "PageStats")
        dgStats.DataSource = ds.Tables("PageStats")
    End Sub

    Private Sub chkHTMLContent_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkHTMLContent.CheckStateChanged
        tpHTML.Enabled = chkHTMLContent.Checked
        tpInnhold.Enabled = Not chkHTMLContent.Checked
        tpBilde.Enabled = Not chkHTMLContent.Checked
        'btnHTMLSection.Enabled = chkHTMLContent.Checked
    End Sub

    Private Sub btnEditUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditUser.Click, lbUsers.DoubleClick
        pUser.Visible = True
        PopulateCategories(lbUserMaingroup, 0)
        PopulateCategories(lbUserMainCat, 3)
        GetUser(lbUsers.SelectedValue)
    End Sub

    Private Sub btnBrowsePicture_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowsePicture.Click
        If OpenFileDialog1.ShowDialog = DialogResult.OK Then
            If imgSection.Image Is Nothing Or (Not imgSection.Image Is Nothing And MessageBox.Show("Vil du erstatte bilde?", "Erstatte bilde", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2, MessageBoxOptions.DefaultDesktopOnly) = DialogResult.Yes) Then
                Dim img As Bitmap = Image.FromFile(OpenFileDialog1.FileName)
                imgSection.Image = img
                btnRemovePicture.Enabled = Not imgSection.Image Is Nothing
            End If
        End If
    End Sub

    Private Sub btnDeleteSection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteSection.Click
        If MessageBox.Show("Er du sikker p� at du vil slette denne seksjonen?", "Slett seksjon", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2, MessageBoxOptions.DefaultDesktopOnly) = DialogResult.Yes Then
            daSection.SelectCommand.Parameters.Item("@id").Value = lbSections.SelectedValue
            dtSection = New DataTable
            daSection.Fill(dtSection)
            dtSection.Rows(0).Delete()
            daSection.Update(dtSection)
            pSection.Visible = False
            PopulateSections(lbSections)
        End If
    End Sub

    Private Sub btnRemovePicture_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemovePicture.Click
        If MessageBox.Show("Er du sikker p� at du vil slette dette bilde?", "Slette bilde", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2, MessageBoxOptions.DefaultDesktopOnly) = DialogResult.Yes Then
            imgSection.Image = Nothing
            btnRemovePicture.Enabled = Not imgSection.Image Is Nothing
        End If
    End Sub

    Private Sub btnSaveUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveUser.Click
        If createdBy.Text <> "" Then
            SaveUser()
        Else
            MessageBox.Show("Feltet for hvem som har opprettet/endret brukeren m� fylles ut", "Felter mangler", MessageBoxButtons.OK, MessageBoxIcon.Error)
            createdBy.Focus()
        End If

    End Sub

    Private Sub btnNewUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewUser.Click
        pUser.Visible = True
        GetUser(-1)
        PopulateCategories(lbUserMaingroup, 0)
        PopulateCategories(lbUserMainCat, 3)
    End Sub

    Private Sub btnDeleteUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteUser.Click
        If MessageBox.Show("Er du sikker p� at du vil slette denne brukeren?", "Slett bruker", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2, MessageBoxOptions.DefaultDesktopOnly) = DialogResult.Yes Then
            daUser.SelectCommand.Parameters.Item("@id").Value = lbUsers.SelectedValue
            dtUser = New DataTable
            daUser.Fill(dtUser)
            dtUser.Rows(0).Delete()
            daUser.Update(dtUser)
            pUser.Visible = False

            If onlySG.Checked Then
                PopulateUsers(lbUsers, 0, cbShowUserSG.SelectedValue)
            Else
                PopulateUsers(lbUsers)
            End If
        End If
    End Sub

    Private Sub btnRefreshUsers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshUsers.Click
        If onlySG.Checked Then
            PopulateUsers(lbUsers, lbUsers.SelectedValue, cbShowUserSG.SelectedValue)
        Else
            PopulateUsers(lbUsers, lbUsers.SelectedValue)
        End If
    End Sub

    Private Sub btnCancelUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelUser.Click
        pUser.Visible = False
        curruser = -1
    End Sub

    Private Sub MainForm_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        End
    End Sub

    Private Sub btnFindUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFindUser.Click

        If onlySG.Checked Then
            PopulateUsers(lbUsers, 0, cbShowUserSG.SelectedValue)
        Else
            PopulateUsers(lbUsers)
        End If

        lbUsers.SelectedIndex = lbUsers.FindString(txtSearchUser.Text)
    End Sub

    Private Sub btnShowArticles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShowArticles.Click
        Dim da As New SqlDataAdapter("_GetEditAppArticles", dbNTB)
        Dim ds As New DataSet

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        da.SelectCommand.Parameters.Add(New SqlParameter("@maingroup", SqlDbType.VarChar, 100))
        da.SelectCommand.Parameters.Item("@maingroup").Value = ""
        da.SelectCommand.Parameters.Add(New SqlParameter("@interval", SqlDbType.Int))
        da.SelectCommand.Parameters.Item("@interval").Value = cbInterval.Text
        da.SelectCommand.Parameters.Add(New SqlParameter("@source", SqlDbType.VarChar, 20))
        da.SelectCommand.Parameters.Item("@source").Value = "ArticlesAdmin"
        da.Fill(ds, "Articles")
        dgArticles.DataSource = ds.Tables("Articles")
    End Sub

    Private Sub btnDeleteArticle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteArticle.Click
        If dgArticles.CurrentRowIndex <> -1 Then
            If MessageBox.Show("Er du sikker p� at du vil slette denne artikkelen? ('" & CType(dgArticles.DataSource, DataTable).Rows(dgArticles.CurrentRowIndex)("articletitle") & "')", "Slett artikkel", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2, MessageBoxOptions.DefaultDesktopOnly) = DialogResult.Yes Then
                Dim cd As New SqlCommand("delete from articles where refid = @refid", dbNTB)
                cd.Parameters.Add(New SqlParameter("@refid", SqlDbType.Int))
                dbNTB.Open()
                cd.Parameters.Item("@refid").Value = CType(dgArticles.DataSource, DataTable).Rows(dgArticles.CurrentRowIndex)("RefId")
                Try
                    Dim cnt As Integer
                    cnt = cd.ExecuteNonQuery()
                Catch ex As Exception
                End Try
                btnShowArticles_Click(Nothing, Nothing)
                dbNTB.Close()
            End If
        End If
    End Sub

    Private Sub btnShowUserStats_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShowUserStats.Click
        Dim da As New SqlDataAdapter("_GetUserStats", dbNTB)
        Dim ds As New DataSet
        Dim uid As Integer

        If rbUsUser.Checked Then uid = cbUsers.SelectedValue Else uid = -1
        da.SelectCommand.CommandType = CommandType.StoredProcedure

        da.SelectCommand.Parameters.Add(New SqlParameter("@userid", SqlDbType.Int, 4))
        da.SelectCommand.Parameters.Item("@userid").Value = uid


        da.SelectCommand.Parameters.Add(New SqlParameter("@fromdate", SqlDbType.VarChar, 8))
        da.SelectCommand.Parameters.Item("@fromdate").Value = Format(dtFromDate3.Value, dtFromDate3.CustomFormat)
        da.SelectCommand.Parameters.Add(New SqlParameter("@todate", SqlDbType.VarChar, 8))
        da.SelectCommand.Parameters.Item("@todate").Value = Format(dtToDate3.Value.AddDays(1), dtToDate3.CustomFormat)

        Try
            da.Fill(ds, "UserStats")
        Catch ex As Exception
        End Try

        dgUserStats.DataSource = ds.Tables("UserStats")

    End Sub

    Private Sub Expire_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Expire.CheckedChanged
        ExpireDate.Enabled = Expire.Checked
    End Sub

End Class

Public Class usrItem
    Private lbl As String
    Private idx As Integer

    Public Property label() As String
        Get
            Return lbl
        End Get
        Set(ByVal Value As String)
            lbl = Value
        End Set
    End Property

    Public Property id() As Integer
        Get
            Return idx
        End Get
        Set(ByVal Value As Integer)
            idx = Value
        End Set
    End Property

End Class
